case $PWD in
    "$HOME/src")
            alias=(
                "get-emacs" "git clone git://git.sv.gnu.org/emacs.git"
                "get-wesnoth" "git-svn clone -r 38796 http://svn.gna.org/svn/wesnoth/trunk wesnoth"
                "get-gnash" "bzr branch http://bzr.savannah.gnu.org/r/gnash/trunk/gnash"
                "get-conkeror" "git clone git://repo.or.cz/conkeror.git"
                "get-git" "git clone git://git.kernel.org/pub/scm/git/git.git"
                "get-bzr" "bzr branch http://bazaar-vcs.org/bzr/bzr.dev/"
                "get-xmonad" "darcs get http://code.haskell.org/xmonad && darcs get http://code.haskell.org/XMonadContrib"
            );;

    "$HOME/src/elisp")
            alias=(
                "get-org-mode" "git clone git://repo.or.cz/org-mode.git"
                "get-emms" "git clone git://git.sv.gnu.org/emms.git"
                "get-emacs-w3m" "cvs -d :pserver:anonymous@cvs.namazu.org:/storage/cvsroot login && cvs -d :pserver:anonymous@cvs.namazu.org:/storage/cvsroot co emacs-w3m"
                "get-slime" "cvs -d :pserver:anonymous:anonymous@common-lisp.net:/project/slime/cvsroot co slime"
                "get-auctex" "cvs -z3 -d:ext:<membername>@cvs.savannah.gnu.org:/sources/auctex co auctex"
                "get-gnus" "echo password is gnus; cvs -d :pserver:gnus@cvs.gnus.org:/usr/local/cvsroot login && cvs -d :pserver:gnus@cvs.gnus.org:/usr/local/cvsroot checkout gnus"
                "get-magit" "git clone git://gitorious.org/magit/mainline.git magit"
                "get-bbdb" "cvs -d :pserver:anonymous@bbdb.cvs.sourceforge.net:/cvsroot/bbdb checkout bbdb"
                "get-yasnippet" "svn co http://yasnippet.googlecode.com/svn/trunk yasnippet"
                "get-haskellmode-emacs" "darcs get http://code.haskell.org/haskellmode-emacs"
            );;

    "$HOME/src/emacs")
            alias=(
                "pull" "git pull"
                "clean" "make maintainer-clean"
                "build" "./configure --prefix=$HOME/usr --with-x-toolkit=no && make && cd lisp && make recompile EMACS=../src/emacs && cd ../ && make"
                "bootstrap" "./configure --prefix=$HOME/usr --with-x-toolkit=no && make bootstrap && make bootstrap"
                "install" "echo Use a PKGBUILD"
                "update" "pwda pull && pwda build && pwda install"
                "update-bootstrap" "pwda pull && pwda clean && pwda bootstrap && pwda install"
            );;

    "$HOME/src/wesnoth")
            alias=(
                "pull" "git-svn rebase"
                "branch-devel" "git checkout master"
                "branch-1.6" "git checkout master-1.6"
                "clean" "scons -c all"
                "build" "scons prefix=$HOME/usr"
                "install" "echo Use a PKGBUILD"
                "update" "pwda clean && pwda build && pwda install"
                "update-1.6" "pwda branch-1.6 && pwda clean && pwda pull && pwda build && pwda install"
                "update-devel" "pwda branch-devel && pwda clean && pwda build && pwda install"
            );;

    "$HOME/src/gnash")
            alias=(
                "clean" "make clean"
                "pull" "bzr pull"
                "build" "./configure --enable-gui=gtk --prefix=$HOME/usr && make"
                "install" "echo Use a PKGBUILD"
                "update" "pwda clean && pwda pull && pwda build && pwda install"
            );;

    "$HOME/src/conkeror")
            alias=(
                "clean" "make clean"
                "pull" "git pull"
                "build" "contrib/build.sh xulapp"
                "install" "echo Use a PKGBUILD"
                "update" "pwda clean && pwda pull && pwda build && pwda install"
            );;

    "$HOME/src/git")
            alias=(
                "clean" "make clean"
                "pull" "git pull"
                "build" "make configure && ./configure --prefix=$HOME/usr && make all doc"
                "install" "echo Use a PKGBUILD"
                "update" "pwda clean && pwda pull && pwda build && pwda install"
            );;

    "$HOME/src/bzr.dev")
            alias=(
                "clean" "make clean"
                "pull" "bzr pull"
                "build" "PREFIX=$HOME/usr make"
                "install" "echo Use a PKGBUILD"
            );;

    "$HOME/src/elisp/org-mode")
            alias=(
                "clean" "make clean"
                "pull" "git pull"
                "build" "make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/haskellmode-emacs")
            alias=(
                "clean" "make clean"
                "pull" "darcs pull"
                "build" "make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/emms")
            alias=(
                "clean" "make clean"
                "pull" "git pull"
                "build" "make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/emacs-w3m")
            alias=(
                "clean" "make clean"
                "pull" "cvs update"
                "build" "make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/slime")
            alias=(
                "pull" "cvs update"
                "update" "pwda pull"
            );;

    "$HOME/src/elisp/auctex")
            alias=(
                "clean" "make clean"
                "pull" "cvs update"
                "build" "./configure && make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/gnus")
            alias=(
                "clean" "make clean"
                "pull" "cvs update"
                "build" "./configure && make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/magit")
            alias=(
                "clean" "make clean"
                "pull" "git pull"
                "build" "./configure && make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/bbdb")
            alias=(
                "clean" "make clean"
                "pull" "cvs update"
                "build" "./configure && make"
                "update" "pwda clean && pwda pull && pwda build"
            );;

    "$HOME/src/elisp/yasnippet")
            alias=(
                "pull" "svn update"
                "build" "rake"
                "update" "pwda pull && pwda build"
            );;

    "$HOME/src/xmonad")
            alias=(
                "clean" "runhaskell Setup.lhs clean"
                "build" "runhaskell Setup.lhs configure --user --prefix=$HOME/usr && runhaskell Setup.lhs build"
                "install" "echo Use a PKGBUILD"
                "pull" "darcs pull"
                "update" "pwda pull && pwda build && pwda install && cd $HOME/src/XMonadContrib && pwda update"
            );;

    "$HOME/src/XMonadContrib")
            alias=(
                "clean" "runhaskell Setup.lhs clean"
                "build" "runhaskell Setup.lhs configure --user --prefix=$HOME/usr && runhaskell Setup.lhs build"
                "install" "echo Use a PKGBUILD"
                "pull" "darcs pull"
                "update" "pwda pull && pwda build && pwda install && cd $HOME/projects/XMonad && pwda update"
            );;

    "$HOME/projects/xmonad-modules")
            alias=(
                "clean" "runhaskell Setup.lhs clean"
                "build" "runhaskell Setup.lhs configure --user && runhaskell Setup.lhs build"
                "install" "echo Use a PKGBUILD"
                "update" "pwda build && pwda install"
                "archive" "git archive --prefix xmonad-modules-$2/ --format tar $2 > ../packages/xmonad-modules/xmonad-modules-$2.tar.gz"
            );;
    "$HOME/projects/maildown")
            alias=(
                "clean" "runhaskell Setup.lhs clean"
                "build" "runhaskell Setup.lhs configure --user && runhaskell Setup.lhs build"
                "install" "echo Use a PKGBUILD"
                "update" "pwda build && pwda install"
                "archive" "git archive --prefix maildown-$2/ --format tar $2 > ../packages/maildown/maildown-$2.tar.gz"
            );;
    "$HOME/projects/packages")
            alias=(
                "clean" "rm -rf **/{pkg,src,*.tar.{gz,bz2}}"
            );;
    "$HOME/src/arch/abs")
            alias=(
                "clean" "rm -rf **/{pkg,src,*.tar.{gz,bz2}}"
            );;
esac
