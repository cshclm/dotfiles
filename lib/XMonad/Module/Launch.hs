-----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Module.Launch
-- Copyright   :  (C) 2017 Chris Mann
-- License     :  GPL3
--
-- Maintainer  :  cshclm@gmail.com
-- Stability   :  unstable
-- Portability :  unportable
--
-- A wrapper around a simple launch-alias script
--
-----------------------------------------------------------------------------

module XMonad.Module.Launch (
                           -- * Usage
                           -- $usage
                           launchPrompt
                         ) where
import XMonad
import XMonad.Core
import XMonad.Prompt
import XMonad.Util.Run
import Data.List.Split

data Launch = Launch

instance XPrompt Launch where
    showXPrompt Launch       = "Goto: "
    commandToComplete _ c = c
    nextCompletion      _ = getNextCompletion

-- | Prompt for an expansion
launchPrompt :: XPConfig -> String -> X ()
launchPrompt c browser = do
  opts <- io $ getOptions browser
  mkXPrompt Launch c (mkComplFunFromList opts) (spawnLauncher browser)

-- | Spawn a command based on the input.
spawnLauncher :: String -> String -> X ()
spawnLauncher browser input =
    spawn (browser ++ " " ++ input)

getOptions :: String -> IO [String]
getOptions browser =
    lines `fmap` runProcessWithInput browser ["--options"] []
