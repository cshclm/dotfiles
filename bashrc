# -*- mode: sh; -*-
# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
# Shell is non-interactive.  Be done now!
    return
fi

if [ -f /etc/bash_completion ]; then
    source /etc/bash_completion
fi

if [ -z $DISPLAY ]; then
    setterm -bfreq 0
fi

if [ -d $HOME/bin ]; then
    PATH=$HOME/bin:$PATH
fi

# Add default git installation path
if [ -d /usr/libexec/git-core ]; then
    PATH=/usr/libexec/git-core:$PATH
fi

# notify of background job completion
set -o notify

# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
if [[ -f ~/.dir_colors ]] ; then
    eval $(dircolors -b ~/.dir_colors)
elif [[ -f /etc/DIR_COLORS ]] ; then
    eval $(dircolors -b /etc/DIR_COLORS)
fi

function mplayerwrapper () {
    # Disable DPMS and blanking
    xset -dpms s off
    mplayer "$@"
    # Enable DPMS and blanking again
    xset +dpms s on
}

function cvlcwrapper () {
    # Disable DPMS and blanking
    xset -dpms s off
    cvlc --play-and-exit "$@"
    # Enable DPMS and blanking again
    xset +dpms s on
}

function mvol () {
    amixer -c 0 sset Master,0 "$1";
}

function glestwrapper () {
    killall unclutter &> /dev/null
    glest
    unclutter -idle 2 -jitter 2 -root &
}

function tremwrapper () {
    killall unclutter &> /dev/null
    tremulous
    unclutter -idle 2 -jitter 2 -root &
}

function zxpdf () {
    temppdf=$(tempfile) || exit
    zcat "$1" > $temppdf && xpdf "$temppdf"
    rm -f -- "$file"
}

export HISTSIZE=15000
export BROWSER="conkeror"
export EDITOR="emacsclient -c -a emacs"
export SBCL_HOME="/usr/local/lib/sbcl/"
export SURFRAW_google_country="au"
export BREAK_CHARS="\"#'(),;\`\\|!?[]{}" # For SBCL

alias ls='ls --color=auto'
alias vlc-nox="vlc -I ncurses"
alias cvlc="cvlcwrapper"
alias glest="glestwrapper"
alias tremulous="tremwrapper"
alias grep='grep --colour=auto'
alias timestamp="date '+%Y%m%d%H%M%S'"
alias pysh="ipython -p pysh"
alias sbcl="rlwrap -b \$BREAK_CHARS sbcl"
alias update="sudo aptitude update"
alias safeupgrade="sudo aptitude safe-upgrade"
alias upgrade="update && safeupgrade"
alias mplayer="mplayerwrapper"

# Multimedia control shortcuts
alias mstop="emacsclient --eval \"(emms-stop)\""
alias mstopf="emacsclient --eval \"(emms-stop-when-finished)\""
alias mplay="emacsclient --eval \"(emms-toggle-playing)\""
alias mnext="emacsclient --eval \"(emms-next)\""
alias mprev="emacsclient --eval \"(emms-previous)\""

if [ "$TERM" == dumb  ]; then
    alias git="git --no-pager"
    function less {
	emacsclient --eval "(progn (find-file \"$1\") (toggle-read-only 1))" > /dev/null
    }
fi

# allow C-s to be i-search instead of stop
stty stop undef

# Initialise X11 on login to tty1.
#if [ "$(tty)" == "/dev/tty1" ]; then
#    emacsen start
#    /usr/bin/xinit
#fi

if [ -n "${SSH_CLIENT}" ]; then
    alias s='screen -RD'
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/chris/.sdkman"
[[ -s "/home/chris/.sdkman/bin/sdkman-init.sh" ]] && source "/home/chris/.sdkman/bin/sdkman-init.sh"

# bashrc ends here

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/usr/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/usr/etc/profile.d/conda.sh" ]; then
        . "/usr/etc/profile.d/conda.sh"
    else
        export PATH="/usr/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

