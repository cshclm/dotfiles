# -*- mode: sh; -*-
umask 022

HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt extended_glob
setopt nohup
setopt notify
setopt hash_list_all
setopt completeinword
setopt auto_pushd
setopt nonomatch
setopt nobeep
setopt append_history
setopt prompt_subst
setopt hist_ignore_all_dups
setopt cdablevars

autoload -U promptinit
promptinit
autoload -U compinit
compinit

autoload -U select-word-style
select-word-style bash

# allow C-s to be i-search instead of stop
stty stop undef

# assign keys using ^[ as the escape char ('\M' does not work on some
# terminals)
bindkey '^[f' emacs-forward-word
bindkey '^W' kill-region
bindkey '^[h' backward-kill-word
bindkey '^[?' run-help

if [ -d /usr/lib/git-core ]; then
    PATH="/usr/lib/git-core:$PATH"
fi

if [ -d /opt/atlassian/bin ]; then
    PATH="/opt/atlassian/bin:$PATH"
fi

if [ -d /opt/homebrew/bin ]; then
    PATH="/opt/homebrew/bin:$PATH"
fi

if [ -d /opt/homebrew/bin ]; then
    PATH="/opt/homebrew/bin:$PATH"
fi


autoload -U zfinit
zfinit

function setprompt() {
    retval=$?
    # 1. Colour opening '(' according to '$?'
    # 2. user(@hostname).  Hostname is only printed if connected remotely
    # 3. PWD.  If the directory is not writable, indicate with red foreground
    # 4. Color closing ')' according to '$?'
    # 5. Prompt terminator.  Red foreground for root
    PROMPT="$(test $retval == 0 && print '%F{green}' || print '%F{red}')(%f\
%n$(test -z $SSH_CLIENT || print '@%m')\
:$(test -w $PWD || print '%F{red}')%1~%f\
$(test $retval -eq 0 && print '%F{green}' || print '%F{red}'))\
$(test $UID -eq 0 && print '%F{red}' || print '%F{green}')%%%f "
    unset retval
}

function precmd() {
    setprompt
}

function wvlc () {
    # Disable DPMS and blanking
    xset -dpms s off
    vlc "$@"
    # Enable DPMS and blanking again
    xset +dpms s on
}

function mvol () {
    amixer -c 0 sset Master,0 "$1";
}

# command_not_found_handler was added in 4.3.5.
#command_not_found_handler() {
#    pwda "$@"
#}

# prevent display being blanked
no_dpms_blanking () {
    xset -dpms s off
    killall xscreensaver &> /dev/null
    $($@)
    # Enable DPMS and blanking again
    xscreensaver &
    xset +dpms s on
}

alias ls='ls --color'
alias timestamp="date '+%Y%m%d%H%M%S'"
alias pysh='ipython -p pysh'
alias sbcl='rlwrap -b \$BREAK_CHARS sbcl'
alias mplayer='mplayerwrapper'
alias vplay='no_dpms_blanking /usr/bin/cvlc --play-and-exit'
alias noblank='no_dpms_blanking'
alias ra='repman add'
alias rr='repman remove'
alias aurman='pbget --aur-only'
alias sshid='withkey ssh-add /media/usbkey/ssh/id_rsa.$(hostname)'
alias perl10='/usr/bin/perl5.10.1'
alias perl12='/usr/local/bin/perl5.12.3'
export WORDCHARS=''
export BREAK_CHARS="\"#'(),;\`\\|!?[]{}"
export PATH="$HOME/.local/bin:$HOME/.go/bin:$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$HOME/projects/jira/bin:$HOME/.cabal/bin:$HOME/src/gradle-3.4/bin:$HOME/projects/atlassian-scripts/bin:$HOME/.rbenv/bin:$HOME/.gem/ruby/2.6.0/bin:$HOME/bin:/usr/local/bin:$PATH:$HOME/cs3231/root/bin"
export EDITOR="emacsclient -c -a emacs"
export VISUAL="emacsclient -c -a emacs"
export INFOPATH=$HOME/media/info/:/usr/share/info/:/usr/local/share/info/
export SSH_ASKPASS="/usr/bin/ssh-askpass"
export PKG_CONFIG_PATH="/usr/local/lib64/pkgconfig:$PKG_CONFIG_PATH"
export LD_LIBRARY_PATH="/usr/local/lib64:$LD_LIBRARY_PATH"
export GOPATH="$HOME/.go"

# Disable the notification system in ruby's notiffany lib which hijacks
# non-guard tests and triggers notifications spuriously
export GUARD_NOTIFY="false"

[ $TERM = "dumb" ] && unsetopt zle && PS1='$ '
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
zstyle ':completion:*:(ssh|scp|sftp|ping):*' hosts cyril.local cybil.local gnawty.local diababa.local

# DISPLAY found in environment; assume X available.
if [ -d "/opt/homebrew" ]; then
    kill-line() { zle .kill-line ; echo -n $CUTBUFFER | pbcopy -i }
    yank() { LBUFFER=$LBUFFER$(pbpaste) }
    zle -N kill-line # bound on C-k
    zle -N yank # bound on C-y
else
    if [ "$DISPLAY" ]; then
        kill-line() { zle .kill-line ; echo -n $CUTBUFFER | wl-copy }
        yank() { LBUFFER=$LBUFFER$(wl-paste) }
        zle -N kill-line # bound on C-k
        zle -N yank # bound on C-y
    fi
fi

# Common aliases for easing use of possibly limited input capabilities from
# remote machine
if [ -n "$SSH_CLIENT" ]; then
    alias v='vctrl'
    alias vp='DISPLAY=":0" vctrl -p'
    alias ec='emacsclient -t'
    alias ecd='emacsclient -t -s desktop'
fi

. "${HOME}/.personal.sh"

export BOOT_JVM_OPTIONS='-Xmx8g -XX:-OmitStackTraceInFastThrow'
#export MAVEN_OPTS="-Xmx3000M -XX:+TieredCompilation -XX:TieredStopAtLevel=1"
export LANG="en_US.UTF-8"

if [ -f $HOME/.host-profile.sh ]; then
    source $HOME/.host-profile.sh
fi

if [ -f $HOME/.laas-zsh-completion ]; then
    source $HOME/.laas-zsh-completion
fi

if [ -e $HOME/.nix-profile ]; then
    $HOME/.nix-profile/etc/profile.d/nix-daemon.sh
fi

#export XKB_DEFAULT_LAYOUT='us'
#export XKB_DEFAULT_VARIANT='dvorak'
#export XKB_DEFAULT_OPTIONS='ctrl:nocaps,altwin:meta_alt'

export _JAVA_AWT_WM_NONREPARENTING=1
export USE_SWAY=0
export USE_HYPRLAND=1
export MOZ_ENABLE_WAYLAND=1

#if [[ $TERM_PROGRAM == "tmux" ]]; then
#    esh
#fi

#if [ -n $USE_SWAY -a "$(tty)" = "/dev/tty1" ]; then
#    export XDG_SESSION_TYPE=wayland
#    export XDG_CURRENT_DESKTOP=sway
#    # For obs-studio & wlrobs (https://hg.sr.ht/~scoopta/wlrobs)
#    #export QT_QPA_PLATFORM=xcb
#    # From https://www.reddit.com/r/swaywm/comments/tze9lw/improve_zoom_on_sway/
#    # Fixes zoom crashing
#    export QT_QPA_PLATFORM="wayland"
#    export QT_QPA_PLATFORMTHEME=qt5ct
#    export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
#
#    ssh-agent sway
#    exit 0
#fi

if [ -n $USE_HYPRLAND -a "$(tty)" = "/dev/tty1" ]; then
    export XDG_SESSION_TYPE=wayland
    export XDG_CURRENT_DESKTOP=hyprland
    # For obs-studio & wlrobs (https://hg.sr.ht/~scoopta/wlrobs)
    #export QT_QPA_PLATFORM=xcb
    # From https://www.reddit.com/r/swaywm/comments/tze9lw/improve_zoom_on_sway/
    # Fixes zoom crashing
    export QT_QPA_PLATFORM="wayland"
    export QT_QPA_PLATFORMTHEME=qt5ct
    export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"

    ssh-agent Hyprland
    exit 0
fi

export PATH="/opt/homebrew/opt/util-linux/bin:$PATH"
