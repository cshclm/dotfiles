#!/bin/sh

KEYMAP_DIR="$HOME/.local/share/keymap"

mkdir -p ${KEYMAP_DIR}

if [ -e "${KEYMAP_DIR}/alt.active" ]; then
    $HOME/bin/keymap-update.sh &
else
    $HOME/bin/keymap-update.sh -n &
fi
