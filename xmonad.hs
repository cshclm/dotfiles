--- xmonad.hs --- xmonad configuration

import XMonad

import qualified XMonad.StackSet as W

import XMonad.Prompt
import XMonad.Prompt.Ssh
import XMonad.Prompt.Shell
import XMonad.Prompt.Window

import XMonad.Util.Run(spawnPipe, safeSpawn)
import XMonad.Util.WorkspaceCompare
import XMonad.Util.NamedWindows

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.UrgencyHook

import qualified XMonad.Actions.Submap as SM
import XMonad.Actions.Warp
import XMonad.Actions.WindowGo
import Graphics.X11.ExtraTypes.XF86

import System.Exit
import System.IO

import Control.Arrow (first)

import qualified Data.Map        as M
import Data.List (isSuffixOf)
import Data.Monoid (mempty)

import XMonad.Module.Launch
import XMonad.Util.EZConfig

import XMonad.Hooks.SetWMName

data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
    urgencyHook LibNotifyUrgencyHook w = do
        name     <- getName w
        Just idx <- fmap (W.findTag w) $ gets windowset
        safeSpawn "notify-send" [show name, "workspace " ++ idx]

customTerminal           = "/usr/bin/urxvt"

customFocusFollowsMouse  = False
customNormalBorderColor  = "black"
customFocusedBorderColor = "grey22"

customXPKeymap =
  -- control + <key>
  M.fromList $ map (first $ (,) controlMask)
  [ (xK_u,          killBefore)
  , (xK_k,          killAfter)
  , (xK_a,          startOfLine)
  , (xK_e,          endOfLine)
  , (xK_y,          pasteString)
  , (xK_d,          deleteString Next)
  , (xK_n,          moveHistory W.focusUp')
  , (xK_h,          moveHistory W.focusDown')
  , (xK_p,          moveHistory W.focusDown')
  , (xK_b,          moveCursor Prev)
  , (xK_f,          moveCursor Next)
  , (xK_w,          killWord Prev)
  , (xK_g,          quit)
  , (xK_q,          quit)
  ] ++
  -- Alt + <key>
  map (first $ (,) mod1Mask)
  [ (xK_f,          moveWord Next)
  , (xK_b,          moveWord Prev)
  , (xK_Delete,     killWord Next)
  , (xK_d,          killWord Next)
  , (xK_h,          killWord Prev)
  , (xK_BackSpace,  killWord Prev)
  ] ++
  map (first $ (,) 0)
  [ (xK_Return,     setSuccess True >> setDone True)
  , (xK_KP_Enter,   setSuccess True >> setDone True)
  , (xK_BackSpace,  deleteString Prev)
  , (xK_Delete,     deleteString Next)
  , (xK_Left,       moveCursor Prev)
  , (xK_Right,      moveCursor Next)
  , (xK_Home,       startOfLine)
  , (xK_End,        endOfLine)
  , (xK_Down,       moveHistory W.focusUp')
  , (xK_Up,         moveHistory W.focusDown')
  , (xK_Escape,     quit)
  ]

customXPConfig = def {
                   font              = "xft:DeJa Vu Sans Mono-9:antialias=True:pixelsize=11"
                 , promptKeymap      = customXPKeymap
                 , promptBorderWidth = 0
                 }

customWorkspaces =
  map (\c -> c:[]) $ '-' : ['a'..'z']

customModMask     = mod4Mask
customNumlockMask = mod2Mask

customKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    [ ((modMask,                 xK_g          ), windowPromptGoto customXPConfig)
    , ((modMask .|. controlMask, xK_g          ), windowPromptBring customXPConfig)
    , ((modMask .|. controlMask, xK_c          ), spawn $ XMonad.terminal conf)
    , ((modMask,                 xK_a          ), spawn "emacsclient --eval '(init-emms-toggle-playing)' &")
    , ((modMask,                 xK_o          ), spawn "emacsclient --eval '(init-emms-stop-when-finished)' &")
    , ((modMask,                 xK_e          ), spawn "emacsclient --eval '(emms-stop)' &")
    , ((modMask,                 xK_q          ), spawn "emacsclient --eval '(emms-queue-goto-next-track)' &")
    , ((modMask,                 xK_semicolon  ), spawn "emacsclient --eval '(emms-previous)' &")
    , ((modMask,                 xK_period     ), spawn "amixer -c 0 sset Master,0 3+ 2>&1 /dev/null &")
    , ((modMask,                 xK_comma      ), spawn "amixer -c 0 sset Master,0 3- 2>&1 /dev/null &")
    , ((modMask,                 xK_apostrophe ), spawn "pactl set-sink-mute 0 toggle &")
    , ((modMask .|. controlMask, xK_apostrophe ), spawn "amixer -c 0 sset Headphone,0 unmute &")
    , ((0      ,                 xK_F1         ), spawn "/home/chris/bin/switchkeymap &")
    , ((0      ,                 xK_F5         ), spawn "/home/chris/bin/keymap-update.sh &")
    , ((modMask,                 xK_F11        ), spawn "sudo xbacklight -dec 10 &")
    , ((modMask,                 xK_F12        ), spawn "sudo xbacklight -inc 10 &")
    , ((0      ,                 xF86XK_ScreenSaver ), spawn "xscreensaver-command -lock &")
    , ((0      ,                 xK_Pause ), spawn "/home/chris/bin/dock.sh -x &")
    , ((modMask,                 xK_r          ), shellPrompt customXPConfig)
    , ((modMask,                 xK_s          ), launchPrompt customXPConfig "launch")
    , ((modMask .|. controlMask, xK_s          ), prompt "helm-google.sh" customXPConfig)
    , ((modMask .|. controlMask, xK_z          ), sshPrompt customXPConfig)
    , ((modMask,                 xK_z          ), shellPrompt customXPConfig)
    , ((modMask,                 xK_x          ), sendMessage $ ToggleStrut U)
    , ((modMask,                 xK_l          ), spawn "sleep 0.5; xscreensaver-command -lock &")
    , ((modMask .|. controlMask, xK_l          ), spawn "sleep 0.5; xset s activate; xscreensaver-command -lock &")
    , ((modMask,                 xK_BackSpace  ), spawn "sudo systemctl suspend")
    , ((controlMask,             xK_Insert     ), spawn "xvkbd -xsendevent -text \"$(xsel)\"")
    , ((modMask,                 xK_f          ), withFocused $ windows . W.sink)
    , ((modMask,                 xK_j          ), windows W.focusMaster)
    , ((modMask,                 xK_p          ), windows W.focusUp)
    , ((modMask,                 xK_u          ), windows W.focusDown)
    , ((modMask,                 xK_k          ), windows W.swapMaster)
    , ((modMask,                 xK_y          ), windows W.swapUp)
    , ((modMask,                 xK_i          ), windows W.swapDown)
    , ((modMask,                 xK_space      ), sendMessage NextLayout)
    , ((modMask .|. controlMask, xK_space      ), setLayout $ XMonad.layoutHook conf)
    , ((modMask,                 xK_b          ), sendMessage (IncMasterN (-1)))
    , ((modMask,                 xK_m          ), sendMessage (IncMasterN 1))
    , ((modMask,                 xK_v          ), sendMessage Expand)
    , ((modMask,                 xK_w          ), sendMessage Shrink)
    , ((modMask .|. controlMask, xK_d          ), kill)
    , ((modMask,                 xK_d          ), banish LowerRight)
    , ((modMask,                 xK_Escape     ), io (exitWith ExitSuccess))
    , ((modMask,                 xK_Return     ), spawn "killall dzen2; killall trayer; xmonad --recompile; xmonad --restart")
    ]
    ++
    -- mod-{n,h}, Switch to physical/Xinerama screens 1, 2
    -- mod-ctrl-{n,h}, Move client to screen 1, 2
    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_h, xK_n] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, controlMask)]]
    
workSpaceKeys =
  map (\k -> ("M-t " ++ k, windows $ W.greedyView k)) customWorkspaces ++
  map (\k -> ("M-t C-" ++ k, windows $ W.shift k)) customWorkspaces ++
  [
    ("M-c e", runOrRaise "emacsclient-emacs" (className =? "Emacs"))
  , ("M-c c", runOrRaise "tmux-wrap" (className =? "kitty"))
  , ("M-c f", runOrRaise "firefox" (className =? "Firefox"))
  , ("M-c s", spawn "/home/chris/bin/scandoc &")
  , ("C-M-p p", spawn "/home/chris/bin/display.sh -p &")
  , ("C-M-p n", spawn "/home/chris/bin/display.sh -n &")
  , ("C-M-p e", spawn "/home/chris/bin/display.sh -e &")
  , ("C-M-p l", spawn "/home/chris/bin/display.sh -l &")
  , ("C-M-p o", spawn "/home/chris/bin/display.sh -o &")
  , ("C-M-p m", spawn "/home/chris/bin/display.sh -m &")
  ]

customMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $
    -- alt-button1, Set the window to floating mode and move by dragging
    [ ((mod4Mask, button1), (\w -> focus w >> mouseMoveWindow w
                                          >> windows W.shiftMaster))
    -- alt-button2, Raise the window to the top of the stack
    , ((mod4Mask, button2), (\w -> focus w >> windows W.shiftMaster))
    -- alt-button3, Set the window to floating mode and resize by dragging
    , ((mod4Mask, button3), (\w -> focus w >> mouseResizeWindow w
                                          >> windows W.shiftMaster))
    ]

customLayout = tiled ||| Mirror tiled ||| Full
  where
     tiled   = Tall nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100

customManageHook = composeAll
    [ className =? "MPlayer"               --> doFloat
    , className =? "emulator-arm"          --> doFloat
    , resource  =? "desktop_window"        --> doIgnore
    , resource  =? "stalonetray"           --> doIgnore
    , className =? "tremfusion"            --> unfloat
    , className =? "glest"                 --> unfloat
    , className =? "wesnoth"               --> unfloat
    , className =? "wesnoth-dev"           --> unfloat
    -- Window placement
    , className =? "Emacs"                 --> doShift "a"
    , className =? "XTerm"                 --> doShift "o"
    , className =? "URxvt"                 --> doShift "o"
    , className =? "com-mathworks-util-PostVMInit"   --> doShift "x"
    , className =? "Conkeror"              --> doShift "u"
    , className =? "Slack"                 --> doShift "c"
    , className =? "Firefox"               --> doShift "u"
    , className =? "Next"                  --> doShift "u"
    , className =? "chromium"              --> doShift "u"
    , className =? "Chromium"              --> doShift "u"
    , className =? "Google-chrome"         --> doShift "u"
    , className =? "Google-chrome-unstable" --> doShift "u"
    , className =? "chromium-browser"      --> doShift "u"
    , className =? "Tor Browser"           --> doShift "u"
    , className =? "Uzbl-tabbed"           --> doShift "u"
    , className =? "jetbrains-idea"        --> doShift "r"
    , className =? "Xpdf"                  --> doShift "i"
    , className =? "Evince"                --> doShift "i"
    , className =? "Djview"                --> doShift "i"
    , className =? "XDvi"                  --> doShift "i"
    , name      =? "VLC (XVideo output)"   --> doShift "m"
    , name      =? "Ario"                  --> doShift "m"
    , className =? "kodi.bin"              --> doShift "m"
    , name      =? "Output"                --> doShift "z"
    , name      =? "!Windows 8 VM"         --> doShift "v"
    , className =? "Vlc"                   --> doShift "m"
    , className =? "Remmina"               --> doShift "r"
    , className =? "vlc"                   --> doShift "m"
    , className =? "MPlayer"               --> doShift "m"
    , className =? "display"               --> doShift "m"
    , className =? "Pithos"                --> doShift "m"
    , className =? "tremfusion"            --> doShift "g"
    , className =? "glest"                 --> doShift "g"
    , className =? "wesnoth"               --> doShift "g"
    , className =? "wesnoth-dev"           --> doShift "g"
    , className =? "Crossfire-client-gtk2" --> doShift "g"
    , className =? "Gimp"                  --> doShift "d"
    , className =? "Inkscape"              --> doShift "d"
    , className =? "Pidgin"                --> doShift "c"
    , className =? "HipChat"               --> doShift "c"
    , className =? "Thunderbird"           --> doShift "e"
    , className =? "VirtualBox"            --> doShift "v"
    , manageDocks
    ]
    where name    = stringProperty "WM_NAME"
          unfloat = ask >>= doF . W.sink
          role = stringProperty "WM_WINDOW_ROLE"

customLogHook dzen = dynamicLogWithPP $ dzenPP
                        {
                          ppCurrent = dzenColor "lightskyblue1" "grey30"
                        , ppTitle   = dzenColor "lightskyblue1" "" . shorten 60
                        , ppVisible = dzenColor "lightskyblue3" ""
                        , ppHidden  = dzenColor "grey50" ""
                        , ppUrgent  = dzenColor "yellow" "red"
                        , ppSep     = " . "
                        , ppLayout  = dzenColor "lightskyblue4" ""
                        , ppWsSep   = ""
                        , ppOutput  = hPutStrLn dzen
                        }

defaults dzen = def {
    terminal           = customTerminal
    , focusFollowsMouse  = customFocusFollowsMouse
    , modMask            = customModMask
    , workspaces         = customWorkspaces
    , normalBorderColor  = customNormalBorderColor
    , focusedBorderColor = customFocusedBorderColor
    , keys               = customKeys
    , mouseBindings      = customMouseBindings
    , startupHook        = setWMName "LG3D"
    , layoutHook         = avoidStruts $ customLayout
    , manageHook         = customManageHook
    , logHook            = customLogHook dzen
    } `additionalKeysP` workSpaceKeys

main = do
  dzen <- spawnPipe "toolbar left"
  dzensys <- spawnPipe "toolbar right"
  xmonad $ withUrgencyHook LibNotifyUrgencyHook $ docks $ defaults dzen
