Default is a profile name for notmuch that contains hooks (among other things) that are invoked by notmuch hen in the expected location.

`$XDG_CONFIG_HOME/notmuch/default/hooks`

See man pages: `NOTMUCH-CONFIG(1)` and `NOTMUCH-HOOKS(5)`
