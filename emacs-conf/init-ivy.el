;;; init-ivy.el: Ivy-mode configuration

;; Copyright (C) 2020 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'counsel)
(require 'ivy)
(require 'swiper)
(require 'avy)

(and (init-depends "keybindings")
     (global-set-key (kbd "C-c h") 'counsel-ag)
     (global-set-key (kbd "C-s") 'isearch-forward)
     (global-set-key (kbd "C-r") 'isearch-backward)
     (global-set-key (kbd "C-x b") 'counsel-switch-buffer)
     (global-set-key (kbd "C-x C-f") 'counsel-find-file)
     (global-set-key (kbd "M-x") 'counsel-M-x)
     (global-set-key (kbd "M-y") 'counsel-yank-pop)
     (global-set-key (kbd "<f1> f") 'counsel-describe-function)
     (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
     (global-set-key (kbd "<f1> l") 'counsel-find-library)
     (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
     (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
     (global-set-key (kbd "<f2> j") 'counsel-set-variable)
     (global-set-key (kbd "C-c C-M-r") 'ivy-resume)
     (global-set-key (kbd "C-c -") 'ivy-push-view)
     (global-set-key (kbd "C-c _") 'ivy-pop-view))

(define-key counsel-find-file-map (kbd "C-.") 'counsel-up-directory)
(define-key ivy-mode-map (kbd "C-.") 'counsel-up-directory)

(set-face-attribute 'avy-lead-face nil
                    :background "unspecified"
                    :foreground "magenta"
                    :bold t)

(require 'key-chord)
(key-chord-define-global "BB" 'counsel-switch-buffer)
(key-chord-define-global "GG" 'counsel-ag)
(key-chord-define-global ",." 'counsel-projectile-find-file)
(key-chord-define-global ".p" 'counsel-projectile-switch-to-buffer)
(key-chord-define-global ",," 'counsel-projectile-switch-project)
(key-chord-mode 1)

(setcdr (assoc 'counsel-M-x ivy-initial-inputs-alist) "")
(setcdr (assoc 'org-refile ivy-initial-inputs-alist) "")
(setcdr (assoc 'org-agenda-refile ivy-initial-inputs-alist) "")

(ivy-mode 1)
(projectile-global-mode)

(setq ivy-height 20
      ivy-wrap t
      avy-keys (number-sequence ?a ?z)
      ivy-use-virtual-buffers t)

(require 'orderless)
(setq completion-styles '(orderless basic partial-completion)
      completion-category-overrides '((file (styles basic partial-completion))))

(setq ivy-re-builders-alist '((t . orderless-ivy-re-builder)))
(add-to-list 'ivy-highlight-functions-alist '(orderless-ivy-re-builder . orderless-ivy-highlight))

;;; init-ivy.el ends here

