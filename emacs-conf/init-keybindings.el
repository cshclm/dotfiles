;;; init-keybindings.el --- Configuration for custom global keybindings

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(find-function-setup-keys)

;;(global-set-key (kbd "C-a") 'init-move-beginning-of-line)
;;(global-set-key (kbd "C-e") 'init-move-end-of-line)
(define-key help-map "a" 'apropos)
(global-set-key (kbd "C-x C-h") help-map)
(global-set-key (kbd "C-h") 'previous-line)
(global-set-key (kbd "M-p") 'backward-kill-word)
(global-set-key (kbd "C-M-y") 'browse-kill-ring)

(global-set-key (kbd "C-c l") 'init-load)
(global-set-key (kbd "M-p") 'mark-paragraph)
(global-set-key (kbd "M-h") 'backward-kill-word)

(global-set-key (kbd "<insert>") 'yank)

(defvar ctl-c-d-map (make-sparse-keymap)
  "Keymap for subcommands of C-c d.")
(defalias 'ctl-c-d-prefix ctl-c-d-map)

(when (string= system-type "gnu/linux")
  (global-set-key (kbd "C-c d <f10>") (lambda () (interactive) (async-shell-command "scandoc")))
  (global-set-key (kbd "C-c d s") (lambda () (interactive) (async-shell-command "launch-dmenu ss")))
  (global-set-key (kbd "C-c d z") (lambda () (interactive) (async-shell-command "sudo /usr/local/bin/suspend"))))

(when (init-depends "functions")
  (global-set-key (kbd "C-c e") 'init-pretty-print-eval))

(add-hook 'asm-mode-hook
          (lambda ()
            (define-key asm-mode-map (kbd "C-c C-c") 'compile)))

(global-unset-key (kbd "C-<down-mouse-1>"))

;;; init-keybindings.el ends here
