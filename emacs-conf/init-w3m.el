;;; init-w3m.el --- Configuration for w3m

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(unless (> emacs-major-version 21)
  (error "%s" "Minimum supported version is Emacs 22"))

(add-to-list 'load-path (expand-file-name "emacs-wget" init-library-path))
(add-to-list 'load-path (expand-file-name "w3m" init-library-path))

(defvar init-apropos-url-alist
      '(("^gw " . "http://www.google.com/search?q=%s")
        ("^g! " . "http://www.google.com/search?btnI=I%27m+Feeling+Lucky&q=%s")
        ("^gi " . "http://images.google.com/images?sa=N&tab=wi&q=%s")
        ("^gg " . "http://groups.google.com/groups?q=%s")
        ("^gd " . "http://www.google.com/search?&sa=N&cat=gwd/Top&tab=gd&q=%s")
        ("^gn " . "http://news.google.com/news?sa=N&tab=dn&q=%s")
        ("^s " . "http://www.slashdot.org")
        ("^ss " . "http://www.osdn.com/osdnsearch.pl?site=Slashdot&query=%s")
        ("^fm " . "http://www.freshmeat.net")
        ;; fix this
        ("^e " . "http://www.emacswiki.org/cgi-bin/wiki?search=%s")
        ("^e" . "http://www.emacswiki.org")
        ("^hayoo " . "http://holumbus.fh-wedel.de/hayoo/hayoo.html?query=%s")
        ("^hoogle " . "http://haskell.org/hoogle/?hoogle=%s"))
      "List of regular expressions matching searchable web sites.")

(defvar init-browse-apropos-site-for-buffer "gw"
  "Default site to use with `init-browse-apropos-site-for-buffer'.")

(autoload 'wget "wget" "Wget interface to download URI asynchronously." t)

(setq wget-download-directory "~/"
      browse-url-browser-function 'w3m-browse-url
      browse-url-new-window-flag t
      w3m-default-directory "~/"
      w3m-use-cookies t
      w3m-default-save-directory "~/"
      w3m-enable-google-feeling-lucky nil
      w3m-pop-up-windows nil
      w3m-use-symbol nil
      w3m-use-title-buffer-name t
      w3m-search-default-engine "google"
      w3m-new-session-in-background t
      w3m-favicon-type nil
      browse-url-generic-program "xdg-open"
      browse-url-browser-function '(("." . browse-url-generic))
      w3m-search-engine-alist
      '(("googleau" "http://www.google.com.au/search?q=%s&hl=en&ie=utf-8&meta=cr=countryAU" utf-8)
	("google" "http://www.google.com.au/search?q=%s&ie=utf-8" utf-8)
	("google-en" "http://www.google.com/search?q=%s&hl=en&ie=utf-8" utf-8)
	("google news" "http://news.google.co.jp/news?hl=ja&ie=utf-8&q=%s" utf-8)
	("google news-en" "http://news.google.com/news?hl=en&q=%s")
	("google groups" "http://groups.google.com/groups?q=%s")
	("technorati" "http://www.technorati.com/search/%s" utf-8)
	("debian-pkg" "http://packages.debian.org/cgi-bin/search_contents.pl?directories=yes&arch=i386&version=unstable&case=insensitive&word=%s")
	("debian-bts" "http://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=yes&pkg=%s")
	("emacswiki" "http://www.emacswiki.org/cgi-bin/wiki?search=%s")
	("en.wikipedia" "http://en.wikipedia.org/wiki/Special:Search?search=%s")
	("freshmeat" "http://freshmeat.net/search/?q=%s&section=projects")))

(add-hook 'w3m-load-hook
	  #'(lambda ()
	      (define-key w3m-mode-map (kbd "\C-ct") 'w3m-copy-buffer)
	      (define-key w3m-mode-map (kbd "o") 'w3m-view-this-url-new-session)
	      (require 'w3m-wget)
	      ;; Reset `w3m-content-type-alist' to prevent inheritance
	      ;; of incompatible browse-url-browser-function setting.
	      (setq w3m-content-type-alist
		    (append (list '("text/html" "\\.s?html?\\'"
				    browse-url-generic nil))
			    (remove (assoc "text/html"
					   w3m-content-type-alist)
				    w3m-content-type-alist)))))

(defun init-convert-postcode-to-state-au (postcode)
  (cl-case (/ postcode 1000)
    (0 "NT/UNKNOWN")
    (1 "UNKNOWN")
    (2 "NSW")                        ; ACT is also classed as NSW in this case
    (3 "VIC")
    (4 "QLD")
    (5 "SA")
    (6 "WA")
    (7 "TAS")))

(and (init-depends "localisation")
     (init-depends "personal")
     (defun init-whitepages-search (resp &optional name state-or-postcode)
       "Search whitepages for NAME at STATE-OR-POSTCODE.
If RESP is non-nil, search business records.  Otherwise, search
residential records."
       (interactive "P\nsName: \nsState/Postcode: ")
       (let ((state (if (= (string-to-number (if (string= "" state-or-postcode)
                                                 (personal-postcode)
                                               state-or-postcode))
                           0)
                        state-or-postcode
                      (init-convert-postcode-to-state-au
                       (string-to-number state-or-postcode))))
             (search-lambda (lambda (url)
                              (when (string-match "whitepages.com.au" url)
                                ;; widen buffer when narrowed
                                (when (> (point-min) 0)
                                  (widen))
                                (narrow-to-region
                                 (if (search-forward-regexp "^Search results"
                                                            (point-max) t)
                                     (progn
                                       (beginning-of-line)
                                       (point))
                                   (point-min))
                                 (save-excursion
                                   (if (search-forward-regexp "^Advertisement"
                                                              (point-max) t)
                                       (progn
                                         (beginning-of-line)
                                         (point))
                                     (point-max))))))))
         (when (string= state-or-postcode "")
               (setq state-or-postcode (personal-postcode)))
         (add-hook 'w3m-display-hook search-lambda)
         (w3m
          (format "http://whitepages.com.au/wp/%sSearch.do?\
subscriberName=%s&state=%s%s"
                  (if resp
                    "res"
                    "bus")
                  name
                  state
                  (if (> (string-to-number state-or-postcode) 0)
                      (concat "&suburb=" state-or-postcode)
                    ""))))))

(defun init-browse-apropos-url (search &optional site)
  "Find SEARCH in SITE.
If SITE is not specified, use `init-browse-apropos-site-for-buffer'."
  (interactive
   (list (let* ((default-search (current-word nil nil))
                (search (read-string
                       (format "Search%s: "
                                (if default-search
                                    (format " (default %s)" default-search)
                                  "")))))
           (if (string= search "")
               (if (string= default-search "")
                   (error "No search terms specified")
                 default-search)
             search))
         (if current-prefix-arg
             (read-string "Location: ")
           init-browse-apropos-site-for-buffer)))
  (let ((searchexp (format "%s %s" site search))
        (aua init-apropos-url-alist))
    (while aua
      (when (string-match (caar aua) searchexp)
        (browse-url (format (cdar aua) search))
        (setq aua nil))
      (setq aua (cdr aua)))))

;;; init-w3m.el ends here
