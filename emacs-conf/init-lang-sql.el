;;; init-sql.el --- SQL configuration

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defcustom init-sql-oracle-data-dictionary
  "select '(\"'||table_name||'\" \"'||column_name||'\")'
   from user_tab_columns
   order by table_name;"
  "SQL Statement to determine all tables and columns."
  :group 'SQL
  :type 'string)

(defvar init-sql-completion-timer nil)

(defvar init-sql-data-dictionary nil
  "The data dictionary to use for completion.
Each element of the list has the form
\(TABLE COLUMN1 COLUMN2 ...)")

(defun init-sql-oracle-data-dictionary ()
  (interactive)
  (setq sql-data-dictionary
	(sql-data-dictionary sql-oracle-data-dictionary))
  (if (not (eq sql-data-dictionary nil))
      (progn
	(cancel-timer init-sql-completion-timer)
	(setq init-sql-completion-timer nil))))

(defvar init-sql-oracle-table-regexp "^(.*)$"
  "Regexp to match output from `sql-oracle-data-dictionary'.")

(defcustom init-sql-mysql-data-dictionary
  "select table_name, column_name from information_schema.columns;"
  "SQL Statement to determine all tables and columns."
  :group 'SQL
  :type 'string)

(defun init-sql-mysql-data-dictionary ()
  "Set the data-dictionary for MySQL."
  (interactive)
  (setq-default sql-data-dictionary
		(sql-data-dictionary sql-mysql-data-dictionary))
  (if (not (eq sql-data-dictionary nil))
      (progn
	(cancel-timer sql-completion-timer)
	(setq sql-completion-timer nil))))

(defvar init-sql-mysql-table-regexp
  "^|.*?\\([A-Za-z_]+\\).*?|.*?\\([A-Za-z_]+\\).*?|"
  "Regexp to match output from `sql-mysql-data-dictionary'.")

(defcustom init-sql-pg-data-dictionary
  "select table_name, column_name from information_schema.columns order by table_name;"
  "SQL Statement to determine all tables and columns."
  :group 'SQL
  :type 'string)

(defun init-sql-pg-data-dictionary ()
  "Set the data-dictionary for Postgres."
  (interactive)
  (setq-default init-sql-data-dictionary
		(init-sql-data-dictionary init-sql-pg-data-dictionary))
  (if (not (eq init-sql-data-dictionary nil))
      (progn
	(cancel-timer init-sql-completion-timer)
	(setq init-sql-completion-timer nil))))

(defvar init-sql-pg-table-regexp
  "^\\ \\([A-Za-z_]+\\).*|\\ \\([A-Za-z_]+\\)"
  "Regexp to match output from `sql-pg-data-dictionary'.")

(defun init-sql-select-regexp ()
  (cond
   ((eq sql-interactive-product 'oracle)
    init-sql-oracle-table-regexp)
   ((eq sql-interactive-product 'postgres)
    init-sql-pg-table-regexp)
   ((eq sql-interactive-product 'mysql)
    init-sql-mysql-table-regexp)))

(defun init-sql-determine-data-dictionary ()
  (cond
   ((eq sql-interactive-product 'oracle)
    init-sql-oracle-data-dictionary)
   ((eq sql-interactive-product 'postgres)
    init-sql-pg-data-dictionary)
   ((eq sql-interactive-product 'mysql)
    init-sql-mysql-data-dictionary)))

(defun init-sql-data-dictionary (statement)
  "Return table and columns from the specified data dictionary using SQL.
STATEMENT must be a SQL statement that returns the data
dictionary one column per line.  Each line must look like this:

\(\"table-name\" \"column-name\")

Any lines not looking like this will be skipped to allow for column
headers and other fancy markup.

This currently depends very much on a good `comint-prompt-regexp'."
  (when (null sql-buffer)
    (error "No SQLi buffer available"))
  (save-excursion
    (set-buffer sql-buffer)
    (let ((regexp (init-sql-select-regexp))
	  result)
      (comint-previous-prompt 1)
      (while (not (= (point) (point-max)))
	(when (looking-at regexp)
	  (let* ((entry (car (read-from-string
			      (concat "\(\""
				      (match-string 1) "\" \""
				      (match-string 2) "\"\)"))))
		 (table (car entry))
		 (column (cadr entry))
		 (item (cdr (assoc table result))))
	    (if item
		(nconc item (list column))
	      (setq result (append (list entry) result)))))
	(forward-line 1))
      result)))

(defun init-sql-complete ()
  (interactive)
  (let ((completions (apply 'append sql-data-dictionary)))
    (and (comint-word "A-Za-z_")
	 (comint-dynamic-simple-complete
	  (comint-word "A-Za-z_")
	  completions))))

(defun init-sql-complete-table ()
  (interactive)
  (let ((completions (mapcar 'car sql-data-dictionary)))
    (and (comint-word "A-Za-z_")
	 (comint-dynamic-simple-complete
	  (comint-word "A-Za-z_")
	  completions))))

(defun init-sql-build-data-dictionary ()
  (let ((function nil))
    (cond
     ((eq sql-interactive-product 'oracle)
      (setq function 'init-sql-oracle-data-dictionary))
     ((eq sql-interactive-product 'postgres)
      (setq function 'init-sql-pg-data-dictionary))
     ((eq sql-interactive-product 'mysql)
      (setq function 'init-sql-mysql-data-dictionary)))
    (comint-simple-send sql-buffer (init-sql-determine-data-dictionary))
    ;; Timer activated as comint-simple-send does not complete immediately
    (setq sql-completion-timer (run-with-timer 3 0.5 function))))

;;(add-hook 'sql-interactive-mode-hook
;;	  '(lambda ()
;;	     (init-sql-build-data-dictionary)
;;	     (define-key sql-interactive-mode-map "\C-i" 'sql-complete)))

;;; init-sql.el ends here
