;; init-webdev.el --- Web development environment for Emacs.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "coffee-mode" init-library-path))
(autoload 'coffee-mode "coffee-mode" "CoffeeScript major mode" t)

(autoload 'tidy-buffer "tidy" "Run Tidy HTML parser on current buffer" t)
(autoload 'tidy-parse-init-file "tidy" "Parse the `tidy-init-file'" t)
(autoload 'tidy-save-settings "tidy" "Save settings to `tidy-init-file'" t)
(autoload 'web-mode "web-mode" "Multi-mode web editing" t)
(autoload 'slim-mode "slim-mode" "Major mode for editing slim templates" t)
(autoload 'js2-minor-mode "js2-mode" "JavaScript (linting) mode" t)
(autoload 'flow-js2-mode "flow-js2-mode" "Flow support for JS2" t)

(load (expand-file-name "js2-mode.el" init-library-path))
(load (expand-file-name "rjsx-mode.el" init-library-path))

(require 'nodejs-repl)
(require 'json-mode)
(require 'prettier-js)
(require 'web-mode)

(defvar ecss-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-c") 'init-ecss-compile)
    map)
  "ECSS keymap.")

(defun init-ecss-value-from-variable ()
  "Replace variable references with their value."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward
            "^[\t ]*\\(@[a-zA-Z0-9_-]+\\):[\t ]*\\(.+\\)[\t ]*;[\t ]*$"
            (point-max) t)
      (save-excursion
        (replace-string (match-string-no-properties 1)
                        (match-string-no-properties 2) nil
                        (point) (point-max)))
      ;; Remove variable definitions.
      (let ((kill-whole-line t))
        (beginning-of-line)
        (kill-line)))))

(defun init-ecss-evaluate-forms ()
  "Evaluate forms and replace the form with the value returned."
  (save-excursion
    (while (search-forward "@(" (point-max) t)
      (forward-char -2)
      (delete-char 1)
      (let ((end-sexp (save-excursion (forward-sexp)
                                      (point-marker))))
        ;; Evaluate and remove evaluated form and surrounding newlines created
        ;; by `eval-region'.
        (save-excursion
          (eval-region (point) end-sexp (current-buffer))
          (delete-backward-char 1)
          (delete-region (point) end-sexp))
        (delete-char 1)))))

(defun init-ecss-compile ()
  "Produce a CSS file after processing ECSS extensions.

ECSS, or Elisp CSS, is a simple extension language for CSS.

There are currently two utilities provided by ECSS: variables and
form evaluations.
All variables assignments are in the form '@<variable-name>: <value>;'.
For example:

@background: #ABC;
@foreground: #123;
@width: 800;

The value of these variables can then be referenced using an '@'
prefix.  For example:

#header {
   background-color: @background;
   foreground-color: @foreground;
}

Would become:

#header {
   background-color: #ABC;
   foreground-color: #123;
}

Forms to be evaluated have the syntax '@(FORM)'.  For example:

#header {
   width: @(+ @width 50)px;
}

Would evaluate to:

#header {
   width: 850px;
}"
  (interactive)
  ;; Create a new file to be used for the converted output.
  (let ((buffer (current-buffer))
        (fname (format "%s%s.%s"
                     (file-name-sans-extension (buffer-name))
                     ;; Minimise chance of an original .css file being
                     ;; overwritten.
                     (if (and (buffer-file-name)
                              (string= (file-name-extension
                                        (buffer-file-name)) "css"))
                         ".ecss"
                       "")
                     "css")))
    (with-temp-file fname
      ;; Use the contents of the ecss buffer.
      (message "Generating %s..." fname)
      (insert-buffer-substring buffer)
      (goto-char (point-min))
      (init-ecss-value-from-variable)
      (init-ecss-evaluate-forms)
      (message "Generating %s...done" fname))))

(define-derived-mode ecss-mode css-mode "ECSS"
  "A major-mode to generate CSS from a template."
  (use-local-map ecss-mode-map))

(setq php-file-patterns nil
      php-completion-file
      (expand-file-name "php-completions.txt" user-emacs-directory)
      js2-ignored-warnings '("msg.extra.trailing.comma")
      prettier-js-args (list "--config" (expand-file-name ".prettierrc" init-work-frontend-monolith-path)))

(add-hook 'webdev-mode-hook (lambda () (rng-auto-set-schema)))

;;(require 'flycheck-flow)
(flycheck-add-mode 'javascript-eslint 'rjsx-mode)
;;(flycheck-add-mode 'javascript-flow 'rjsx-mode)
(flycheck-may-enable-checker 'javascript-eslint)

(defun init-eslint-fix-file ()
  "Run eslint --fix the current buffer."
  (interactive)
  (call-process-shell-command (concat flycheck-javascript-eslint-executable " --fix " (buffer-file-name))
                              nil "*Shell Command Output*" t)
  (revert-buffer t t))

(defun init-eslint-fix-on-save ()
  "Wrapper for running eslint --fix on save."
  (when (eq major-mode 'rjsx-mode)
    (message "Running eslint --fix")
    (init-eslint-fix-file)))

(add-hook 'after-save-hook 'init-eslint-fix-on-save)

(add-hook 'javascript-mode-hook
          (lambda ()
            (define-key js-mode-map (kbd "C-c f") 'init-eslint-fix-file)
            (define-key js-mode-map (kbd "C-c C-r") 'rjsx-mode)
            (define-key js-mode-map (kbd "C-c C-z") 'nodejs-repl-switch-to-repl)
            (define-key js-mode-map (kbd "C-c C-c") 'nodejs-repl-send-region)))

(add-hook 'nxml-mode-hook
          (lambda ()
            (define-key nxml-mode-map (kbd "C-c C-c")
              'init-nxml-insert-dwim)
            (require 'flyspell)
            (add-to-list 'flyspell-prog-text-faces 'nxml-text-face)
            (flyspell-prog-mode)
            (define-key flyspell-mode-map (kbd "M-TAB") nil)))

(add-to-list 'auto-mode-alist '("\\.less'" . css-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.hbs?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.soy?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.emblem\\'" . slim-mode))
(add-to-list 'auto-mode-alist '("\\.coffee\\'" . coffee-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js-mode))
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))
(add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-to-list 'auto-mode-alist (cons (concat init-work-frontend-monolith-path "/.*\\.js\\'") 'rjsx-mode))
(add-hook 'json-mode-hook 'flymake-json-load)
(add-hook 'json-mode-hook 'flycheck-mode)
(setq flycheck-javascript-eslint-executable "jfe-eslint")
(add-hook 'rjsx-mode-hook
          (lambda ()
            (flow-minor-mode 1)
            (prettier-js-mode 1)
            (flycheck-mode 1)
            (define-key rjsx-mode-map (kbd "C-c C-s") 'flow-minor-status)
            (define-key rjsx-mode-map (kbd "C-c C-f")
              (lambda ()
                (interactive)
                (save-buffer)
                (shell-command (format "jfe-eslint %s --fix" buffer-file-name))))))

(add-hook 'js-mode-hook 'js2-minor-mode)

(flycheck-add-mode 'javascript-eslint 'web-mode)
(setq-default flycheck-disabled-checkers
  (append flycheck-disabled-checkers
    '(javascript-jshint)))

(add-hook 'coffee-mode-hook
	  #'(lambda ()
	      (define-key coffee-mode-map (kbd "C-c C-c")
	        'coffee-compile-buffer)
              (define-key coffee-mode-map (kbd "C-c C-e")
                'coffee-compile-region)
              (define-key coffee-mode-map (kbd "C-c C-z")
                'coffee-repl)
              ;; get rid of annoying 'dedent' behaviour
              (define-key coffee-mode-map (kbd "<backspace>")
                'backward-delete-char)))

(setq web-mode-engines-alist '(("php" . "\\.phtml\\'")
                               ("ctemplate" . "\\.html\\'"))
      flycheck-check-syntax-automatically '(save mode-enabled))


(defun init-tide-setup ()
  (tide-setup)
  (flycheck-mode 1)
  (prettier-js-mode 1)
  (eldoc-mode 1)
  (tide-hl-identifier-mode 1))

(add-hook 'web-mode-hook
          (lambda ()
            (when (string-equal "tsx" (file-name-extension buffer-file-name))
              (typescript-mode)
              (init-tide-setup))))

(add-hook 'typescript-mode-hook #'init-tide-setup)

;;; init-webdev.el ends here
