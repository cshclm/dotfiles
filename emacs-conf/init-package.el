;;; init-package.el --- Configuration for ELPA

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;(straight-use-package 'use-package)

(require 'package)

;;(add-to-list 'package-archives
;;             '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("elpa" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives
            '("melpa-stable" . "https://melpa.org/packages/"))
;;(add-to-list 'package-archives
;;             '("melpa" . "https://melpa.org/packages/"))

(setq package-selected-packages
      '(ace-jump-mode
        ace-window
        auctex
        auto-complete
        bbdb
        cider-spy
        confluence
        emacs-eclim
        emms
        enh-ruby-mode
        evil-dvorak
        flow-minor-mode
        flycheck-flow
        flycheck-irony
        flycheck-pos-tip
        groovy-mode
        haskell-mode
        ivy
        counsel
        swiper
        ht
        ido-ubiquitous
        import-js
        inf-clojure
        inf-groovy
        inf-ruby
        javap
        js2-mode
        key-chord
        ledger-mode
        monky
        nodejs-repl
        org-caldav
        org-jira
        org-pomodoro
        org-trello
        ox-reveal
        pomodoro
        prettier-js
        queue
        rainbow-delimiters
        rbenv
        request-deferred
        rjsx-mode
        robe
        rubocop
        scala-mode2
        slime
        smartparens
        solarized-theme
        twittering-mode
        typescript-mode
        w3m
        web-mode
        yaml-mode
        yard-mode))

;;; init-package.el ends here
