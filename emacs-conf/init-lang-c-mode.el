;;; init-c-mode.el --- Configurations for CC Mode.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'doc-mode "doc-mode" "Minor mode for editing in-code documentation."
  t)

(defun init-string-in-path-p (string)
  "Determine whether a substring exists in `buffer-file-name'.
Return non-nil if it exists, otherwise return nil."
  (and buffer-file-name
       (string-match string buffer-file-name)))

(defun init-c-set-project-style ()
  "Set style-conventions for the current buffer based on path."
  (cond ((init-string-in-path-p "src/wesnoth")
         (c-set-style "stroustrup")
         (setq c-basic-offset 4)
         (setq tab-width 4))
        ((init-string-in-path-p "src/emacs")
         (c-set-style "gnu")
         (setq c-basic-offset 2))
        ((init-string-in-path-p "projects/cs3231")
         (c-set-style "bsd")
         (setq indent-tabs-mode t)
         (setq c-basic-offset 8))))

(defun init-c-mode-hook ()
  "Customisations to styles and key bindings in c-mode."
  (define-key c-mode-map "\C-c\C-c" 'compile)
  (define-key c-mode-map (kbd "\C-c\;") 'comment-region)
  (define-key c-mode-map (kbd "M-p") 'backward-list)
  (define-key c-mode-map (kbd "M-n") 'forward-list)
  (irony-mode 1)
  (flycheck-mode 1)
  (init-c-set-project-style))

(require 'irony)
(require 'flycheck-irony)
(flycheck-irony-setup)

(defun init-c++-mode-hook ()
  "Customisations to styles and key bindings in c++-mode."
  (define-key c++-mode-map "\C-c\C-c" 'compile)
  (define-key c++-mode-map (kbd "\C-c\;") 'comment-region)
  (init-c-set-project-style))

(add-hook 'c-mode-hook 'init-c-mode-hook)
(add-hook 'c++-mode-hook 'init-c++-mode-hook)

(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

(setq-default c-electric-flag t)

(setq c-default-style '((java-mode . "java")
                        (awk-mode . "awk")
                        (c-mode . "stroustrup")
                        (other . "k&r")))

;;; init-c-mode.el ends here
