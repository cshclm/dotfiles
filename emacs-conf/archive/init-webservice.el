;;; init-webservice.el --- Configuration to integrate with web services

(add-to-list 'load-path (expand-file-name "twittering-mode" init-library-path))
(autoload 'twit "twittering-mode" "Twitter client" t)
(setq twittering-use-master-password t)

(add-hook 'twittering-mode-hook
          (lambda ()
            (define-key twittering-mode-map "n" 'twittering-goto-next-status)
            (define-key twittering-mode-map "p" 'twittering-goto-previous-status)
            (define-key twittering-mode-map "h" 'twittering-goto-previous-status)))

(add-hook 'twittering-edit-mode-hook (lambda () (flyspell-mode)))

(setq twittering-initial-timeline-spec-string '(":direct_messages" ":home"))

;;; init-webservice.el ends here
