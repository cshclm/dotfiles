;;; init-autoinsert.el --- Configuration for automatically inserting text

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'auto-insert-mode "autoinsert" "Insert a template when a new file is created." t)

(defun init-autoinsert-prepare ()
  "Replace a particular string with the relevant information."
  (interactive)
  (when (buffer-modified-p)
    (save-excursion
      (goto-char 0)
      (while (search-forward "$year$" nil t)
	(replace-match (format-time-string "%Y" (current-time))))
      (goto-char 0)
      (while (search-forward "$buffer$" nil t)
	(replace-match (buffer-name))))
    (when (re-search-forward "^-!-" (point-max) t)
      (replace-match ""))))

(defadvice auto-insert (after
			update-template
			first nil activate)
  "Convert an arbitary string to the current year."
  (init-autoinsert-prepare))

(defun ca-with-comment (str)
  (format "%s%s%s" comment-start str comment-end))

(define-skeleton latex-letter-skeleton
  "Skeleton for a letter in LaTeX."
  nil
  "%% " (buffer-name) "
%% Time-stamp: <>

\\documentclass{letter}
\\usepackage{a4}
%% Variation of \\opening to position text on the left-hand side.
\\newcommand*{\\leftopening}[1]{
  \\ifx\\@empty\\fromaddress
  \\thispagestyle{firstpage}%
  \\@date\\par%
  \\else  % home address
    \\thispagestyle{empty}%
      \\fromaddress \\\\[2\\parskip]%
      \\today\\par%
  \\fi
  \\vspace{2\\parskip}%
  \\toname \\\\ \\toaddress \\par%
  \\vspace{2\\parskip}%
  #1\\par\\nobreak}

%% Variation of \\closing to always position closing on the left-hand side.
\\newcommand{\\leftclosing}[1]{\\par\\nobreak\\vspace{\\parskip}%
  \\stopbreaks
  \\noindent
  \\parbox{\\indentedwidth}{\\raggedright
       \\ignorespaces #1\\\\[6\\medskipamount]%
       \\ifx\\@empty\\fromsig
           \\fromname
       \\else \\fromsig \\fi\\strut}%
   \\par}

\\begin{document}
\\begin{letter}{Name \\& Address}
  \\address{"
  (let (result '())
    (dolist (line personal-postal-address result)
      (setq result
	    (concat result
		    (format "%s%s"
			    (if (not (string= line (car personal-postal-address)))
				"\\\\\n" (concat personal-formal-name "\\\\\n"))
			    line)))))
  "}
  \\signature{" personal-formal-name "}
  \\leftopening{Dear Sir/Madam,}

  " _ "

  \\leftclosing{Yours Sincerely,}
\\end{letter}
\\end{document}\n")

;;; init-autoinsert.el ends here
