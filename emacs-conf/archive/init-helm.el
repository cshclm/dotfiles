;;; init-helm.el: Helm-mode configuration

;; Copyright (C) 2015 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;(add-to-list 'load-path (expand-file-name "emacs-async" init-library-path))
;;(add-to-list 'load-path (expand-file-name "helm" init-library-path))
(require 'helm)
(require 'helm-config)
;;(require 'helm-swoop)

(and (init-depends "keybindings")
     (global-set-key (kbd "C-c h") 'helm-do-ag)
     (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
     (global-set-key (kbd "C-x b") 'helm-mini)
     (global-set-key (kbd "C-x C-f") 'helm-find-files)
     (global-set-key (kbd "M-x") 'helm-M-x)
     (global-set-key (kbd "M-y") 'helm-show-kill-ring))

(setq helm-move-to-line-cycle-in-source t
      helm-ff-file-name-history-use-recentf t
      helm-multi-swoop-edit-save t)

(eval-after-load 'helm-mode
  (lambda ()
    (define-key helm-find-files-map (kbd "C-.") 'helm-find-files-up-one-level)
    (set-face-background 'helm-selection "grey20")))

(require 'key-chord)
(key-chord-define-global "BB" 'helm-mini)
(key-chord-define-global "GG" 'helm-do-ag)
(key-chord-define-global ",." 'helm-projectile-find-file)
(key-chord-define-global ".p" 'helm-projectile-switch-to-buffer)
(key-chord-define-global ",," 'helm-projectile-switch-project)
(key-chord-mode 1)

(helm-mode 1)
(helm-autoresize-mode 1)
(projectile-global-mode)
(helm-projectile-on)

;;; init-helm.el ends here
