;;; init-gnus.el --- Gnus configuration

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'gnus-group-make-nnir-group "nnir" "Create an nnir group." t)
(require 'gnus)

(defvar init-gnus-activity-file "/tmp/gnus-activity"
  "File which store notifications relating to Gnus.")

(defface init-gnus-few-face
  '((t (:foreground "grey60")))
  "Few unread messages in the group."
  :group 'gnus-group)

(defface init-gnus-moderate-face
  '((t (:foreground "grey75")))
  "Moderate amount of unread messages in the group."
  :group 'gnus-group)

(defface init-gnus-many-face
  '((t (:foreground "grey90")))
  "Many unread messages in the group."
  :group 'gnus-group)

(defun init-message-tab ()
  "Wrapper around `message-tab' to allow mail alias completion."
  (interactive)
  (if (looking-back "[[:blank:],]\\(\\![[:alnum:]]+\\)")
    (progn
      (looking-back "[[:blank:],]\\(\\![[:alnum:]]+\\)")
      (let ((string (match-string 1))
            (origpos (point))
            (start (match-beginning 1))
            (end (match-end 1)))
        (mail-abbrev-insert-alias string)
        (if (> (point) origpos)
            (delete-region start end)
          ;; for some reason mail-abbrev-complete-alias is slow to return if
          ;; it's the sole match, so try this last
          (mail-abbrev-complete-alias)
          (init-message-tab))))
    (message-tab)))

(defun init-message-work-related-p ()
  "Return non-nil if the current message is work-related."
  (string-match
   personal-employer-regexp (or (save-excursion
                                  (set-buffer gnus-article-buffer)
                                  (message-fetch-field "to"))
                                "")))

(defun init-gnus-notify-important-unread ()
  "Provide notification of new messages."
  (interactive)
  (let ((groups gnus-newsrc-alist)
        (unread 0))
    (while groups
      (when (and (<= (gnus-group-level (caar groups)) 1)
                 (numberp (gnus-group-unread (caar groups))))
        (setq unread (+ (gnus-group-unread (caar groups)) unread)))
      (setq groups (cdr groups)))
    (and (init-depends "notify")
         (init-notify
          ;; summary
          "gnus"
          ;; body
          (format "%d" unread)
          ;; importance
          (> unread 0)
          ;; Options following are specific to init-notify-file-xmobar.
          ;; colourise
          t
          ;; file
          init-gnus-activity-file))))

(defun init-gnus-kill-emacs-on-exit ()
  "Quit Gnus, save any buffers and kill Emacs."
  (interactive)
  (gnus-group-save-newsrc)
  (if (and (fboundp 'daemonp) (daemonp))
      (save-buffers-kill-terminal)
    (gnus-group-exit)
    (save-buffers-kill-emacs)))

(defun init-gnus-save-and-bury-buffer ()
  "Save Gnus newsrc before burying the group buffer."
  (interactive)
  (gnus-group-save-newsrc)
  (bury-buffer))

(init-alias-new "mail" 'gnus)

;; Slightly modified `gnus-demon-scan-news' to allow level to be specified.
;; Allows fetching of priority groups (i.e. email), as `gnus-demon-scan-news'
;; does not update the group buffer.  Also removed several levels of
;; `save-excursion', as it was causing point to move to `point-min'.  How
;; ironic.
(defun init-gnus-demon-scan-news (level)
  "Fetch new news for groups whose priority is higher than LEVEL.
Higher priorities are numerically less."
  (let ((win (current-window-configuration))
        (gnus-verbose 3)
        (gnus-verbose-backends 3))
    (unwind-protect
	(save-window-excursion
          (when (gnus-alive-p)
            (set-buffer gnus-group-buffer)
            (gnus-group-get-new-news level)))
      (set-window-configuration win))))

(and (init-depends "keybindings")
     (global-set-key (kbd "C-c g") 'gnus))

(setq message-send-mail-function 'smtpmail-send-it
      gnus-gcc-mark-as-read t
      compose-mail-user-agent-warnings nil
      gnus-directory (expand-file-name "gnus/" user-emacs-directory)
      gnus-logo-colors '("#c64e3b" "grey30")
      gnus-dribble-directory gnus-directory
      nnml-directory "~/.mail"
      gnus-kill-files-directory gnus-directory
      nnml-active-file (expand-file-name "active" nnml-directory)
      message-directory nnml-directory
      mail-source-directory (expand-file-name "mail" nnml-directory)
      message-auto-save-directory (expand-file-name "drafts" gnus-directory)
      footnote-spaced-footnotes nil
      gnus-treat-display-smileys nil
      gnus-demon-timestep 60
      gnus-activate-level 2
      gnus-always-read-dribble-file t
      gnus-group-highlight '(((> unread 100) . init-gnus-moderate-face)
                             ((> unread 500) . init-gnus-many-face)
                             (t . init-gnus-few-face))
      nnir-search-engine 'namazu
      nnir-namazu-index-directory (expand-file-name "~/.namazu")
      nnir-namazu-remove-prefix (expand-file-name "~/.mail")
      nnir-mail-backend gnus-select-method
      gnus-secondary-select-methods
      (if (string= (init-get-hostname) init-work-hostname)
          '((nnmaildir "atlassian"
                       (directory "~/.mail/atlassian")
                       (directory-files nnheader-directory-files-safe))
            (nnmaildir "mairix"
                       (directory "~/.mail/mairix")
                       (directory-files nnheader-directory-files-safe)))
        '((nnmaildir "gsuite"
                     (directory "~/.mail/gsuite")
                     (directory-files nnheader-directory-files-safe))
          (nnmaildir "mairix"
                       (directory "~/.mail/mairix")
                       (directory-files nnheader-directory-files-safe))))
      gnus-select-method '(nnml "mail"
                                (directory "~/.mail/mail")
                                (directory-files nnheader-directory-files-safe))
      mail-sources (list '(directory :path "~/.mail" :suffix ".spool")
                         `(file :path ,(concat "/var/mail/" user-login-name)))
      gnus-face-10 'shadow
      gnus-summary-line-format
      (concat "%{%U%R%z%}" "%10{|%}" "%1{%d%}" "%10{|%}" "%(%-4,4k%)"
              "%10{|%}" "%(%-24,24f %)" "%10{|%}" "%B" "%s\n")
      mail-user-agent 'gnus-user-agent
      gnus-permanently-visible-groups ".*"
      gnus-interactive-catchup nil
      gnus-sum-thread-tree-single-indent "o "
      gnus-sum-thread-tree-false-root "x "
      gnus-sum-thread-tree-root "* "
      gnus-sum-thread-tree-vertical "| "
      gnus-sum-thread-tree-leaf-with-other "|-> "
      gnus-sum-thread-tree-single-leaf "+-> "
      gnus-sum-thread-tree-indent "  "
      gnus-visible-headers "^\\(To:\\|From:\\|Newsgroups:\\|Cc:\\|Subject:\\|Date:\\)")

(set-default 'gnus-summary-expunge-below -500)

(add-hook 'gnus-article-mode-hook
	  #'(lambda ()
	      (define-key gnus-article-mode-map "\C-x\C-hk"
	        'gnus-article-describe-key)
	      (define-key gnus-article-mode-map "\C-x\C-hc"
	        'gnus-article-describe-key-briefly)))

(defun init-gnus-archive-messages ()
  (interactive)
  (gnus-summary-move-article nil init-gnus-archive-group)
  (gnus-summary-rescan-group))

(add-hook 'gnus-summary-mode-hook
	  #'(lambda ()
              (define-key gnus-summary-mode-map (kbd "'") #'(lambda () (org-capture nil "m")))
              (define-key gnus-summary-mode-map (kbd "C-c C-c") 'init-gnus-archive-messages)))

;;(add-hook 'message-mode-hook (lambda ()
;;                               (init-initialise-bbdb)
;;                               (define-key message-mode-map (kbd "C-i")
;;                                 'init-message-tab)))
(add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)

(add-hook 'kill-emacs-hook
          (lambda () (and (file-exists-p init-gnus-activity-file)
                          (delete-file init-gnus-activity-file))))

(add-hook 'gnus-group-mode-hook
	  #'(lambda ()
              (require 'nnmairix)
              (nnmairix-group-mode-hook)
              (gnus-topic-mode)
	      (define-key gnus-group-mode-map "GG" 'gnus-group-make-nnir-group)
              (init-gnus-notify-important-unread)
              (require 'gnus-demon)
              (gnus-demon-add-handler
               (lambda () (init-gnus-demon-scan-news 2)) 1 nil)
              (define-key gnus-group-mode-map "q"
                'init-gnus-save-and-bury-buffer)
              (define-key gnus-summary-mode-map (kbd "B a")
                (lambda ()
                  (interactive)
                  (gnus-summary-move-article nil init-gnus-work-archive-group)))
              (global-set-key (kbd "C-x C-c") 'init-gnus-kill-emacs-on-exit)
              ;; Split reply windows horizontally to maximise viewable message
              ;; content.
              (gnus-add-configuration
               '(reply
                 (vertical 1.0
                           (summary 0.1)
                           (article 0.3)
                           (message 1.0 point))))

              (gnus-add-configuration
               '(reply-yank
                 (vertical 1.0
                           (summary 0.1)
                           (message 1.0 point))))))

(add-hook 'gnus-after-getting-new-news-hook
          'init-gnus-notify-important-unread)
(add-hook 'gnus-summary-exit-hook 'gnus-summary-bubble-group)

;;; init-gnus.el ends here
