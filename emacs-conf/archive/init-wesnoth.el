;;; init-wesnoth.el --- Configuration for wesnoth-mode

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defcustom init-wmllint-path "wmllint"
  "Path to wmllint."
  :type 'string
  :group 'init)

(add-to-list 'load-path (expand-file-name "wesnoth-mode" init-library-path))
(add-to-list 'load-path (expand-file-name "wesnoth-replay" init-library-path))

(defun init-wesnoth-wmllint-file ()
  "Run wmllint on a file."
  (interactive)
  (shell-command (format "%s %s" init-wmllint-path (buffer-file-name)))
  (insert-file-contents (buffer-file-name) nil nil nil t))

(setq wesnoth-root-directory "~/src/wesnoth/"
      wesnoth-addition-file "~/projects/elisp/wesnoth-mode/wesnoth-wml-additions.cfg"
      wesnoth-update-output-directory "~/projects/elisp/wesnoth-mode/")

(add-hook 'wesnoth-mode-hook
	  '(lambda ()
	     (define-key wesnoth-mode-map (kbd "C-c f")
	       'init-wesnoth-wmllint-file)))

(autoload 'wesnoth-mode "wesnoth-mode" "Major mode for editing WML." t)
(add-to-list 'auto-mode-alist '("\\.cfg\\'" . wesnoth-mode))
(autoload 'wesnoth-replay-add "wesnoth-replay" "Add a Wesnoth replay." t)
(autoload 'wesnoth-replay "wesnoth-replay" "View Wesnoth replays." t)

;;; init-wesnoth.el ends here
