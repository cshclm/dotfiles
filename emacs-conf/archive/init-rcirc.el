;;; init-rcirc.el --- Configuration for rcirc

;; Copyright (C) 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'rcirc "rcirc" "Connect to all servers in `rcirc-server-alist'." t)

(defvar init-rcirc-activity-file "/tmp/rcirc-activity"
  "File to contain rcirc activity output.")

(add-hook 'rcirc-mode-hook
          (lambda ()
            (rcirc-track-minor-mode 1)
            ;; Don't shadow my M-o binding
            (define-key rcirc-mode-map (kbd "M-o") nil)
            (defun-rcirc-command part (channel)
              "Part CHANNEL."
              (interactive "sPart channel: ")
              (let ((channel (if (> (length channel) 0) channel target)))
                (rcirc-send-string process (concat "PART " channel))))

            (defun-rcirc-command quit (reason)
              "Send a quit message to server with REASON."
              (interactive "sQuit reason: ")
              (rcirc-send-string process (concat "QUIT :" reason)))))

(setq rcirc-buffer-maximum-lines 1000
      rcirc-log-flag t)

(when (and (boundp 'irc-nick) (stringp irc-nick))
  (setq rcirc-default-nick irc-nick
        rcirc-default-user-name irc-nick
        rcirc-default-full-name irc-nick))

(defun init-rcirc-open-ssh ()
  "Open SSH tunnel and start rcirc."
  (interactive)
  (save-window-excursion
    (shell-command (concat "ssh -fNL 6667:localhost:6667 "
                           personal-irc-proxy " &")))
  (call-interactively 'rcirc))

(when (init-depends "keybindings")
  (global-set-key (kbd "C-c i") 'rcirc))

(when (init-depends "notify")
  (add-hook 'kill-emacs-hook
            (lambda () (and (file-exists-p init-rcirc-activity-file)
                            (delete-file init-rcirc-activity-file))))

  (defun init-rcirc-notify-setup ()
    "Prepare hooks to provide notifications."
    (if (or (eq init-notify-function 'init-notify-file-xmobar)
            (eq init-notify-function 'init-notify-file-dzen))
        (add-hook 'rcirc-update-activity-string-hook
                'init-rcirc-notify-activity)
      (add-hook 'rcirc-print-hooks 'init-rcirc-notify-message)))

  (defun init-rcirc-notify-message (proc sender response target text)
    "Provide detailed notifications for messages."
    (when (and (string= response "PRIVMSG")
               (if (string= (rcirc-server-name proc) (init-get-hostname))
                   (not (or (string= (rcirc-nick proc) sender)
                            (string= sender "root")))
                 (or (string-match "^[^#]" target)
                     (string-match (rcirc-nick proc) text))))
      (init-notify 
       ;; summary
       target
       ;; body
       (format "%s %s" (if (eq init-notify-type 'dbus)
                           (format "&lt;%s%gt;" sender)
                         (format "<%s>" sender)) text)
       ;; important
       t)))

  (defun init-rcirc-notify-activity ()
    "Provide notifications regarding `rcirc' activity."
    (init-notify
     ;; summary
     nil
     ;; body
     rcirc-activity-string
     ;; important
     t
     ;; xmobar-specific
     ;; colourise text properties
     'prop
     ;; file
     init-rcirc-activity-file))

  (init-rcirc-notify-setup))

;;; init-rcirc.el ends here
