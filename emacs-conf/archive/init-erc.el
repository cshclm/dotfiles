;;; init-erc.el --- Configuration for ERC

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'erc "erc" "IRC client for emacs.")
(autoload 'erc-services-mode "erc-services" "Toggle ERC services mode." t)
(add-hook 'erc-mode-hook
	  '(lambda ()
	     (add-to-list 'erc-modules 'truncate)
	     (add-to-list 'erc-modules 'log)
	     (init-erc-set-faces)
	     (erc-services-mode 1)))

(and (boundp 'erc-nick)
     (setq erc-keywords (list erc-nick)))
(setq erc-port 6667
      erc-fill-column 80
      erc-prompt-for-password nil
      erc-log-matches-flag t
      erc-log-channels-directory (expand-file-name "erc-log" user-emacs-directory)
      erc-generate-log-file-name-function 'erc-generate-log-file-name-short
      erc-prompt "=>")
(and (init-depends "keybindings")
     (global-set-key (kbd "C-c e") 'erc))

(defun init-erc-set-faces ()
  "Customise ERC faces."
  (set-face-attribute 'erc-action-face nil
		      :foreground "rosybrown1"
		      :weight 'normal)
  (set-face-attribute 'erc-direct-msg-face nil
		      :foreground "mediumorchid1")
  (set-face-attribute 'erc-input-face nil
		      :foreground "palegreen2")
  (set-face-attribute 'erc-my-nick-face nil
		      :foreground "plum1"
		      :weight 'bold)
  (set-face-attribute 'erc-nick-msg-face nil
		      :foreground "plum1"
		      :weight 'bold)
  (set-face-attribute 'erc-notice-face nil
		      :foreground "lightsteelblue2"
		      :weight 'normal)
  (set-face-attribute 'erc-prompt-face nil
		      :foreground "thistle2"
		      :background "none"
		      :weight 'normal)
  (set-face-attribute 'erc-timestamp-face nil
		      :foreground "thistle2"
		      :weight 'normal))

;;; init-erc.el ends here
