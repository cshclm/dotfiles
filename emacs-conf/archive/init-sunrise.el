;;; init-sunrise.el: Sunrise Commander configuration

;; Copyright (C) 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'sunrise "sunrise-commander" "Orthodox file manager." t)

(add-to-list 'auto-mode-alist '("\\.srvm\\'" . sr-virtual-mode))

;; Free reserved bindings
(add-hook 'sr-mode-hook
          (lambda ()
            (set-face-attribute 'sr-passive-path-face nil
                                :background 'unspecified
                                :foreground "grey70"
                                :height 'unspecified
                                :bold 'unspecified)

            (set-face-attribute 'sr-active-path-face nil
                                :background 'unspecified
                                :foreground 'unspecified
                                :height 'unspecified
                                :bold 'unspecified)
            (define-key sr-mode-map (kbd "C-c t") nil)
            (define-key sr-mode-map (kbd "C-c T") nil)
            (define-key sr-mode-map (kbd "C-c C-t") nil)
            (define-key sr-mode-map (kbd "C-c C-t t") 'sr-term)
            (define-key sr-mode-map (kbd "C-c C-t c") 'sr-term-cd)
            (define-key sr-mode-map (kbd "C-c C-t n") 'sr-term-cd-newterm)))

(global-set-key (kbd "C-c s") 'sunrise)
(global-set-key (kbd "C-c S") 'sunrise-cd)

;;; init-sunrise.el ends here
