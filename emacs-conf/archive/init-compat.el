;;; init-compat.el --- Configuration compatibility

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defun init-tramp-set-method-for-version ()
  "Set `tramp-default-method' depending on version.
If `tramp-version' is 2.1.x or greater, set
`tramp-default-method'.  Otherwise, set it to nil, as
compatibility issues with `ido-mode' appear to exist."
  (string-match "^\\([0-9]+\\)\\.\\([0-9]+\\)\\.[0-9]+"
		tramp-version)
  (let ((trampver (string-to-number (concat (match-string 1 tramp-version)
					    (match-string 2 tramp-version)))))
    (if (< trampver 21)
	(setq tramp-default-method nil)
      (setq tramp-default-method "ssh"))))

;;; init-compat.el ends here
