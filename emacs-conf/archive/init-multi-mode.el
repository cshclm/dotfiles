;;; init-multi-mode.el --- Support for multiple major-modes in a buffer

;; Copyright (C) 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-hook 'multi-indirect-buffer-hook
          (lambda ()
            (setq indent-region-function 'init-multi-mode-indent-region)))

(defun init-multi-mode-indent-region (&optional start end)
  "Indent lines in a `multi-mode' region."
  (interactive "r")
  (save-restriction
    (widen)
    (save-excursion
      (goto-char (point-min))
      (funcall indent-line-function)
      (while (= 0 (forward-line 1))
        (beginning-of-line)
        (back-to-indentation)
        ;; call post-command-hook to update indent-line-function
        (run-hooks 'post-command-hook)
        (funcall indent-line-function)))))

;;; init-multi-mode.el ends here
