;;; init-emms.el: Emms configuration.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(unless (> emacs-major-version 21)
  (error "%s" "Minimum supported version is Emacs 22"))

(defcustom init-emms-follow-current-track t
  "Whether to follow the currently playing track."
  :type 'boolean
  :group 'init)

(add-to-list 'load-path (expand-file-name "emms/lisp" init-library-path))
(add-to-list 'load-path (expand-file-name "emms-queue" init-library-path))
(require 'emms-setup)
(require 'emms-browser)
(require 'emms-history)
(require 'emms-queue)
(require 'spotify)

(defun init-kill-emacs-stop-emms ()
  "Stop Emms if a process is running and kill emacs."
  (interactive)
  (emms-stop)
  (when emms-cache-dirty
    (emms-cache-save))
  ;; Buffers should already be saved
  (if (and (fboundp 'daemonp) (daemonp))
      (save-buffers-kill-terminal)
    (save-buffers-kill-emacs)))

;; Allow addition of tracks from mpd library.
(defmacro init-define-emms-add (name fn)
  "Define a wrapper around to modify the default directory.
NAME is the name of the command to define.  FN is the function
to be called interactively.

If the defined command is called with a prefix-argument, use
`emms-player-mpd-music-directory' as
`emms-source-file-default-directory'."
  `(defun ,name (&optional mpddir)
     (interactive "P")
     (if mpddir
         (let ((emms-source-file-default-directory emms-player-mpd-music-directory))
           (call-interactively ,fn))
       (call-interactively ,fn))))

(init-define-emms-add init-emms-add-file 'emms-add-file)
(init-define-emms-add init-emms-add-directory 'emms-add-directory)
(init-define-emms-add init-emms-add-directory-tree
                      'emms-add-directory-tree)

;; Redefine emms-player-mpd-playable-p to prevent MPD being used for
;; tracks outside of MPD's music directory.
(defun emms-player-mpd-playable-p (track)
  "Return non-nil when we can play this track."
  (and (memq (emms-track-type track) '(file url playlist streamlist))
       (string-match (emms-player-get emms-player-mpd 'regex)
                     (emms-track-name track))
       (string-match (format "^%s" emms-player-mpd-music-directory)
                     (emms-track-name track))
       (condition-case nil
           (progn (emms-player-mpd-ensure-process)
                  t)
         (error nil))))

(defun init-emms-status ()
  "Return the status of Emms."
  ;; volume information
  (require 'volume)
  (let ((tracks 0)
        (curr_track 0)
        (action "Stopped"))
    ;; track information
    (with-current-buffer emms-playlist-buffer
      (save-excursion
        (mapc #'(lambda (item) (setq tracks (1+ tracks)))
              (emms-playlist-tracks-in-region (point-min) (point-max))))
      (setq curr_track (line-number-at-pos emms-playlist-selected-marker)))
    ;; action information
    (dolist (predictate '((emms-player-playing-p "Playing")
                          (emms-player-paused-p "Paused")))
      (unless (null (eval (car predictate)))
        (setq action (car (cdr predictate)))))
    (format "%s (%d%%): %d/%d" action (volume-amixer-get) curr_track tracks)))

(defun init-emms-status-show ()
  "Display the status of Emms."
  (interactive)
  (message "%s | %s"
           (if emms-player-playing-p
               (emms-track-description (emms-playlist-current-selected-track))
             "[Not playing]")
           (init-emms-status)))

(defun init-emms-return-playing ()
  "Display the currently playing track."
  (interactive)
  (message "%s" ))

(add-hook 'kill-emacs-hook (lambda ()
                             (unless (member 'emms-player-mpd emms-player-list)
                                 (emms-stop))
                             (when emms-cache-dirty
                               (emms-cache-save))))

(add-hook 'emms-player-finished-hook
          '(lambda ()
             (with-current-buffer emms-playlist-buffer
               (let ((target
                      (and emms-playlist-mode-selected-overlay
                           (overlay-start
                            emms-playlist-mode-selected-overlay))))
                 (setq init-emms-follow-current-track
                       (and target (= (point) target)))))))

(add-hook 'emms-player-started-hook
          '(lambda ()
             (with-current-buffer emms-playlist-buffer
               (and init-emms-follow-current-track
                    (emms-playlist-mode-center-current)))))

(defun init-emms-return-playing ()
  "Return the name of the currently playing track."
  (if emms-player-playing-p
      (format emms-show-format
              (emms-track-description
               (emms-playlist-current-selected-track)))
    "Nothing playing right now"))

(defun init-emms-stop-when-finished ()
  "Stop playing once the current track is finished."
  (interactive)
  (add-hook 'emms-player-started-hook 'init-emms-stop-when-finished-cleanup t))

(defun init-emms-stop-when-finished-cleanup ()
  "Cleanup the hooks set up by `emms-stop-when-finished'."
  (emms-stop)
  (remove-hook 'emms-player-started-hook 'init-emms-stop-when-finished-cleanup))

(defadvice emms-stop (before init-emms-stop-and-cleanup first nil activate)
  "Remove hooks set by `emms-stop-when-finished'."
  (remove-hook 'emms-player-started-hook 'init-emms-stop-when-finished-cleanup))

(defun init-emms-toggle-playing (&optional arg)
  "Toggle play/paused state.
When ARG is 0, stop the current track.  If non-zero, resume play.
If ARG is not given, toggle play/pause."
  (interactive "P")
  (unless (emms-playlist-current-selected-track)
    (emms-playlist-select-track-at-p (point-min)))
  (if arg
      (if (= arg 0)
          (emms-stop)
        (emms-start))
    (condition-case err
        (progn
          (emms-ensure-player-playing-p)
          (emms-pause))
      (error (emms-start)))))

(defun init-emms-free ()
  "An Emms setup script.
Everything included in the `emms-minimalistic' setup, the Emms
interactive playlist mode, reading information from tagged audio
files, and a metadata cache.  Contains support only for Free
formats and players."
  ;; include
  (emms-minimalistic)
  (require 'emms-playlist-mode)
  (require 'emms-info)
  (require 'emms-info-ogginfo)
  (require 'emms-info-id3v2)
  (require 'emms-info-metaflac)
  (require 'emms-cache)
  ;; setup
  (setq emms-playlist-default-major-mode 'emms-playlist-mode)
  (add-to-list 'emms-track-initialize-functions 'emms-info-initialize-track)
  (when (executable-find emms-info-ogginfo-program-name)
    (add-to-list 'emms-info-functions 'emms-info-ogginfo))
  (when (executable-find emms-info-metaflac-program-name)
    (add-to-list 'emms-info-functions 'emms-info-metaflac))
  (when (executable-find emms-info-id3v2-program)
    (add-to-list 'emms-info-functions 'emms-info-id3v2))
  (setq emms-track-description-function 'emms-info-track-description)
  (emms-cache 1))

(init-emms-free)

(defun init-emms-format-time (seconds)
  "Format SECONDS into minutes and seconds."
  (and seconds
       (format "%01d:%02d" (/ seconds 60) (% seconds 60))))

(defun init-emms-track-description (track)
  "Return a customised description of the current track."
  (let ((artist (emms-track-get track 'info-artist))
        (title (emms-track-get track 'info-title))
        (playing-time (init-emms-format-time
                       (emms-track-get track 'info-playing-time)))
        (album (emms-track-get track 'info-album)))
    (if title
        (concat
         (and artist
              (format "%s - " artist))
         (and album
              (format "%s - " album))
         (format "%s" title)
         (and playing-time
              (format " (%s)" playing-time)))
      (emms-track-simple-description track))))

(defun init-emms-reload-playlist (&optional now)
  "Reload the playlist on change of track
If NOW is non-nil, reload immediately."
  (interactive "P")
  (if now
    (init-emms-reload-playlist-now)
    (add-hook 'emms-player-started-hook
              'init-emms-reload-playlist-now)
    (message "%s" "Playlist to reload at change of track")))

(when (init-depends "functions")
  (defun init-emms-playlist-update-from-library ()
    "Add tracks from the library which are missing in the playlist."
    (interactive)
    (let* ((library (reverse (search-files-recursively
                              emms-source-file-default-directory
                              "\\(\\.mp3\\|\\.ogg\\|\\.flac\\)$")))
           (tracks (init-emms-get-playlist-names)))
      (while tracks
        (setq library (remove (car tracks) library)
              tracks (cdr tracks)))
      (while library
        (emms-add-file (car library))
        (setq library (cdr library))))))

(defun init-emms-get-playlist-names ()
  "Return a list of all file names in the playlist."
  (let ((res '())
        tname)
    (save-excursion
      (set-buffer emms-playlist-buffer)
      (goto-char (point-min))
      (setq tname (emms-track-get (emms-playlist-track-at (point)) 'name))
      (when tname
        (setq res (cons tname res)))
      (while (eq (forward-line 1) 0)
        (setq tname (emms-track-get (emms-playlist-track-at (point)) 'name))
        (when tname
          (setq res (cons tname res)))))
    res))

(defun init-emms-reload-playlist-now ()
  "Reload the default playlist."
  (interactive)
  (remove-hook 'emms-player-started-hook
               'init-emms-reload-playlist-now)
  (with-current-buffer emms-playlist-buffer
    (emms-playlist-clear))
  (emms-add-directory-tree emms-source-file-default-directory)
  (when (featurep 'emms-queue)
    (setq emms-queue-overlay-list nil))
  (emms-random)
  (emms-shuffle))

(setq emms-playlist-buffer-name "*Music*"
      emms-playlist-default-major-mode 'emms-playlist-mode
      emms-source-file-default-directory "~/media/music/"
      emms-history-file (expand-file-name "emms/history" user-emacs-directory)
      emms-show-format "Playing: %s"
      emms-info-report-each-num-tracks 0
      emms-track-description-function 'init-emms-track-description
      emms-player-list '(emms-player-vlc)
      emms-player-next-function 'emms-queue-or-next
      emms-player-vlc-parameters '("--intf=oldrc")
      emms-repeat-playlist t)

(add-to-list 'emms-track-initialize-functions 'emms-info-initialize-track)
(set-face-foreground 'emms-playlist-track-face "grey70")
(set-face-foreground 'emms-playlist-selected-face "palegreen1")

(set-face-attribute 'emms-browser-artist-face nil
                    :foreground "grey60"
                    :weight 'normal
                    :height 1.0)
(set-face-attribute 'emms-browser-album-face nil
                    :foreground "grey60"
                    :weight 'normal
                    :height 1.0)
(set-face-attribute 'emms-browser-track-face nil
                    :foreground "grey60"
                    :weight 'normal
                    :height 1.0)

(defun init-emms-refresh-info ()
  "Simple method of forcefully updating track information."
  (interactive)
  (while (search-forward-regexp "^/" (point-max) t)
    (if (looking-at "\\.mp3$")
        (emms-info-id3v2 (emms-playlist-track-at (point)))
        (emms-info-ogginfo (emms-playlist-track-at (point)))))
  (emms-cache-save))

(defun init-emms-tag-editor-edit ()
  (interactive)
  (require 'emms-tag-editor)
  (emms-tag-editor-edit))

(autoload 'volume "volume" "Tweak your sound card volume." t)

(when (init-depends "keybindings")
  (global-set-key (kbd "C-c m s") 'emms-show)
  (global-set-key (kbd "C-c m m") 'emms-playlist-mode-go)
  (global-set-key (kbd "C-c m f") 'emms-add-file)
  (global-set-key (kbd "C-c m d") 'emms-add-directory)
  (global-set-key (kbd "C-c m t") 'emms-add-directory-tree)
  (when (init-depends "functions")
    (global-set-key (kbd "C-c m u") 'init-emms-playlist-update-from-library))
  (global-set-key (kbd "C-c m p") 'emms-add-m3u-playlist)
  (global-set-key (kbd "C-c m P") 'emms-add-pls-playlist)
  (global-set-key (kbd "C-c m r") 'init-emms-reload-playlist)
  (global-set-key (kbd "C-c m v") 'volume))

(define-key emms-playlist-mode-map "E" 'init-emms-tag-editor-edit)
(define-key emms-playlist-mode-map "v" 'volume)
(define-key emms-playlist-mode-map (kbd "C-c C-a")
  'init-emms-add-track-to-library)

(emms-cache-restore)
(emms-history-load)
(random t)
(emms-shuffle)
(with-current-buffer emms-playlist-buffer
  (emms-playlist-mode-center-current))
(unless (daemonp)
  (global-set-key (kbd "C-x C-c") 'init-kill-emacs-stop-emms))

(init-alias-new "music" 'emms-playlist-mode-go)

(defun init-emms-add-track-to-library ()
  "Add the track at `point' to the library.
Prompt for the Artist, Album and Title"
  (interactive)
  (let ((track (emms-track-get (emms-playlist-track-at (point)) 'name))
        (fields '("Artist" "Album" "Title"))
        (format "flac")
        tags command)
    (mapcar
     (lambda (x)
       (add-to-list 'tags
                    (cons x (read-from-minibuffer (format "%s: " x)))))
     fields)
    (shell-command (init-emms-add-track-build-command track format tags))))

(defun init-emms-add-track-build-command (filename format tag-alist)
  "Build a command to convert FILENAME to FORMAT and assign tags.
TAG-ALIST is an alist of strings in the format (TAGNAME . TAG)."
  (let ((filename-new (format "%s.flac" (file-name-sans-extension filename))))
    (format "/usr/bin/sox \"%s\" \"%s\.%s\" && %s && \
/usr/bin/metaflac --add-replay-gain \"%s\" && /usr/bin/arename \"%s\""
            filename
            (file-name-sans-extension filename)
            format
            (mapconcat
             (lambda (x)
               (format "/usr/bin/metaflac --remove-tag=\"%s\" \"%s\" && \
/usr/bin/metaflac --set-tag=\"%s=%s\" \"%s\""
                       (car x) filename-new (car x) (cdr x) filename-new))
             tag-alist " && ")
            filename-new
            filename-new)))

;;; init-emms.el ends here
