;;; init-muse.el --- Muse configuration

;; Copyright (C) 2011 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "muse-el/lisp" init-library-path))
(add-to-list 'load-path (expand-file-name "muse-blog" init-library-path))

(autoload 'muse-mode "muse-mode"
  "Muse is an Emacs mode for authoring and publishing documents." t)
(autoload 'muse-blog-initiate-entry "muse-blog"
  "Create a new blog entry." t)

(setq muse-blog-root-dir "~/projects/blog")

(add-hook 'muse-mode-hook
          (lambda ()
            (require 'muse-html)
            (require 'muse-latex)
            (require 'muse-texinfo)
            (require 'muse-blog)))

(add-hook 'muse-blog-create-entry-hook
          (lambda ()
            (muse-blog-build-rss
             (expand-file-name "blog.xml" personal-muse-blog-publish-path)
             personal-muse-blog-rss-entries
             personal-muse-blog-name personal-muse-blog-desc
             personal-muse-blog-url)
            (when (init-depends "webdev" t)
              (save-window-excursion
                (find-file-read-only
                 (expand-file-name "css/main.ecss" muse-blog-root-dir))
                (init-ecss-compile)
                (rename-file
                 (expand-file-name "css/main.css" muse-blog-root-dir)
                 (expand-file-name "css/main.css"
                                   personal-muse-blog-publish-path)
                 t)))))

(setq muse-project-alist
      (list
       (list
        "blog" (list (expand-file-name "entries" muse-blog-root-dir)
                     muse-blog-root-dir
                     :default "index")
        (list :base "xhtml"
              :base-url personal-muse-blog-url
              :path personal-muse-blog-publish-path
              :style-sheet "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\"/>"
              :header "~/projects/blog/templates/header.html"
              :footer "~/projects/blog/templates/footer.html"))))

;;; init-muse.el ends here
