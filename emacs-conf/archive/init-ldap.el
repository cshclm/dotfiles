;;; init-ldap.el --- LDAP support and integration.
 
(require 'ldap)
(require 'eudc)

(setq eudc-default-return-attributes 'all
      eudc-strict-return-matches nil
      eudc-inline-query-format '((cn)
                                 (displayName)
                                 (givenName)
                                 (givenName sn)
                                 (email))
      ldap-default-host "localhost"
      ldap-host-parameters-alist
      '(("localhost" base "ou=addressbook,dc=gnawty,dc=local"))
      eudc-inline-expansion-format '("%s <%s>" displayName email)
      ldap-ldapsearch-args (quote ("-tt" "-LLL" "-x"))
      eudc-server-hotlist '(("localhost" . ldap))
      eudc-inline-expansion-servers 'hotlist)

(eudc-set-server "localhost" 'ldap t)

(defun init-eudc-expand-inline ()
  "Wrapper to make `eudc-expand-inline' more transparent."
  (interactive)
  (if (save-excursion
        (beginning-of-line)
        (looking-at "^\\(To\\|From\\|\\([BF]c\\|C\\)c\\): "))
      (progn
        (insert "*")
        (condition-case nil
            (progn
              (eudc-expand-inline)
              (message ""))
          (error (progn (backward-delete-char 1))
                 (message "No match"))))
    (indent-for-tab-command)))

(defun init-ldap-search (search)
  "Wrapper for `ldap-search' to provide a useful command."
  (interactive "sFilter: ")
  (let ((buffer (get-buffer-create "*ldapsearch*")))
    (set-buffer buffer)
    (toggle-read-only 0)
    (erase-buffer)
    (dolist (record (ldap-search search))
      (dolist (attrib record)
        (insert (format "%s: %s\n" (car attrib) (cadr attrib))))
      (insert "\n"))
    (toggle-read-only 1)
    (goto-char (point-min))
    (switch-to-buffer buffer)))

(and (init-depends "keybindings")
     (add-hook 'message-mode-hook
               (lambda ()
                 (define-key message-mode-map
                   (kbd "TAB") 'init-eudc-expand-inline))))

;;; init-ldap.el ends here
