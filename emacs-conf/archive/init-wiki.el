;;; init-wiki.el --- Wiki-editing support

;; Copyright (C) 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'wikipedia-mode "wikipedia-mode"
  "Major mode for editing Wikipedia-style markup." t)

;; Support visual-line-mode using patch provided by Ulrich Müller.
(defun wikipedia-turn-on-longlines ()
  "Turn on longlines-mode if it is defined."
  (cond ((functionp 'visual-line-mode)
         (visual-line-mode 1))
        ((functionp 'longlines-mode)
         (longlines-mode 1))))

(add-hook 'wikipedia-mode-hook
          (lambda ()
            (and (fboundp 'outline-cycle)
                 (define-key wikipedia-mode-map (kbd "<tab>")
                   'outline-cycle))))

(add-to-list 'auto-mode-alist '("\\.wiki\\'" . wikipedia-mode))

;;; init-wiki.el ends here
