;;; init-anything.el --- Anything configuration

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(unless (> emacs-major-version 21)
  (error "%s" "Minimum supported version is Emacs 22"))

(add-to-list 'load-path (expand-file-name "anything" init-library-path))
(add-to-list 'load-path (expand-file-name "anything/anything-config"
                                          init-library-path))

(autoload 'anything "anything" "Select anything" t)

(defvar init-anything-sources-standard
  '(anything-c-source-buffers
    anything-c-source-man-pages
    anything-c-source-info-pages
    anything-c-source-info-elisp
    anything-c-source-emacs-functions
    anything-c-source-emacs-variables
    anything-c-source-locate
    anything-c-source-bookmarks
    anything-c-source-w3m-bookmarks
    anything-c-source-kill-ring
    anything-c-source-file-name-history)
  "Minimal sources for `anything'.")

(defcustom init-anything-c-swish-e-index
  (expand-file-name "~/.index.swish-e")
  "Filename for the swish-e index."
  :type 'file
  :group 'init)

(defvar init-anything-c-source-swish-e
  '((name . "Swish-e")
    (candidates . (lambda ()
                    (apply 'start-process "swish-e-process" nil
                           (append (list "swish-e" "-H" "0" "-x"
                                         "<swishdocpath>\n" "-f"
                                         init-anything-c-swish-e-index
                                         "-w")
                                   (list anything-pattern)))))
    (type . file)
    (requires-pattern . 3)
    (delayed))
  "Source for retrieving files matching the current input pattern
with swish-e.")

;; Custom source definition for BBDB, which also includes aliases.
(defun init-anything-c-bbdb-candidates ()
  "Return a list of all names in the bbdb database.  The format
is \"Firstname Lastname\"."
  (mapcar (lambda (bbdb-record)
            (replace-regexp-in-string
             "\\s-+$" ""
             (format "%s %s %s"
                     (aref bbdb-record 0)
                     (aref bbdb-record 1)
                     (or (aref bbdb-record 2) ""))))
          (bbdb-records)))

(defun init-anything-c-bbdb-string-aka (candidate)
  "Strip any AKA information from CANDIDATE."
  (let ((pos (position ?\( candidate)))
    (if pos
        (subseq candidate 0 (1- pos))
      candidate)))

(defvar init-anything-c-source-bbdb
  '((name . "BBDB")
    (candidates . init-anything-c-bbdb-candidates)
    (volatile)
    (action ("Send a mail" .
             (lambda (candidate)
               (bbdb-send-mail
                (anything-c-bbdb-get-record
                 (init-anything-c-bbdb-string-aka candidate)))))
            ("View person's data" .
             (lambda (candidate)
               (bbdb-redisplay-one-record
                (anything-c-bbdb-get-record
                 (init-anything-c-bbdb-string-aka candidate))))))))

(add-hook 'anything-before-initialize-hook
          (lambda ()
            ;; This seems not to be loaded by anything(-config)
            (require 'bbdb)
            (require 'info)
            (require 'anything-config)
            (setq anything-sources
                  (cons 'init-anything-c-source-bbdb
                        init-anything-sources-standard))
            (define-key anything-map (kbd "C-v") 'anything-next-source)
            (define-key anything-map (kbd "M-v") 'anything-previous-source)))

;;; init-anything.el ends here
