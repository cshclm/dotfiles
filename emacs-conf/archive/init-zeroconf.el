;;; init-zeroconf.el --- Zeroconf support

;; Copyright (C) 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'zeroconf)
(zeroconf-init)

(defun init-zeroconf-browse (&rest servicetypes)
  "Output services available which are providing SERVICETYPES.
If SERVICETYPES is not specified, all services are displayed."
  (interactive "sServices: ")
  (when (interactive-p)
    (setq servicetypes (split-string (car servicetypes))))
  (let ((outbuf (get-buffer-create "*zeroconf*"))
        (map (make-sparse-keymap)))
    (switch-to-buffer outbuf)
    (toggle-read-only t)
    (define-key map (kbd "q") 'bury-buffer)
    (use-local-map map)
    (let ((buffer-read-only nil))
      (erase-buffer)
      (save-excursion
        (dolist (type (or servicetypes (zeroconf-list-service-types)))
          (dolist (service (zeroconf-list-services type))
            (let ((x (zeroconf-resolve-service service)))
              (insert (format "%s (%s) provides '%s' (%s)\n"
                              (zeroconf-service-host x)
                              (zeroconf-service-address x)
                              (zeroconf-service-name x)
                              (zeroconf-service-type x))))))))
    (display-buffer outbuf)))

;;; init-zeroconf.el ends here
