;;; init-ido.el: Ido-mode configuration

;; Copyright (C) 2007, 2008, 2009, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(ido-mode t)
(ido-everywhere t)
(setq ido-create-new-buffer 'always)

(add-hook 'ido-minibuffer-setup-hook
          (lambda ()
            (define-key ido-common-completion-map "\C-n" 'ido-next-match)
            (define-key ido-common-completion-map "\C-p" 'ido-prev-match)
            (define-key ido-common-completion-map "\C-h" 'ido-delete-backward-updir)
            (define-key ido-common-completion-map "\C-s" 'isearch-forward)
            (define-key ido-common-completion-map "\C-r" 'isearch-backward)))

;;; init-ido.el ends here
