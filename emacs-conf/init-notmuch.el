;;; init-notmuch.el --- notmuch configuration

;; Copyright (C) 2021 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'notmuch "notmuch" "notmuch mail" t)

(require 'notmuch)

(and (init-depends "keybindings")
     (global-set-key (kbd "C-c g") 'notmuch))

(define-key notmuch-show-mode-map "C-c S"
  (lambda ()
    "Mark message as spam"
    (interactive)
    (notmuch-show-tag (list "+spam" "-inbox"))))

(define-key notmuch-show-mode-map "A"
  (lambda ()
    "Archive message"
    (interactive)
    (notmuch-show-tag (list "-inbox"))))

(define-key notmuch-show-mode-map (kbd "RET") #'goto-address-at-point)
(define-key notmuch-show-mode-map (kbd "C-c RET") #'notmuch-show-toggle-message)

(eval-after-load 'notmuch
  (lambda ()
    (set-face-foreground 'notmuch-tag-face (alist-get 'mauve catppuccin-macchiato-colors))
    (set-face-foreground 'notmuch-tag-flagged (alist-get 'orange catppuccin-macchiato-colors))
    (set-face-foreground 'notmuch-tag-unread (alist-get 'yellow catppuccin-macchiato-colors))
    (set-face-foreground 'notmuch-tag-deleted (alist-get 'red catppuccin-macchiato-colors))
    (set-face-foreground 'notmuch-tag-added (alist-get 'green catppuccin-macchiato-colors))))
