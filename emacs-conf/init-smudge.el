;;

(require 'smudge)

(global-set-key (kbd "C-c m a") 'smudge-controller-toggle-play)
(global-set-key (kbd "C-c m q") 'smudge-controller-next-track)
(global-set-key (kbd "C-c m ;") 'smudge-controller-previous-track)
(global-set-key (kbd "C-c m s") 'smudge-track-search)
(global-set-key (kbd "C-c m p") 'smudge-playlist-search)
(global-set-key (kbd "C-c m m") 'smudge-my-playlists)
(global-set-key (kbd "C-c m o") (lambda () (async-shell-command "pactl-switch-port")))

(setq smudge-oauth2-callback-port "10678")
