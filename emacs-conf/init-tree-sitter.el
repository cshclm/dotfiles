;; init-tree-sitter.el --- Tree-sitter related configuration.

;; Copyright (C) 2023 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(when (string-match "^29." emacs-version)
  (add-to-list 'load-path (expand-file-name "combobulate" init-library-path))

(use-package treesit
  :preface
  (defun mp-setup-install-grammars ()
    "Install Tree-sitter grammars if they are absent."
    (interactive)
    (dolist (grammar
             '((css "https://github.com/tree-sitter/tree-sitter-css")
               (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript" "master" "src"))
               (python "https://github.com/tree-sitter/tree-sitter-python")
               (emacs-lisp "https://github.com/Wilfred/tree-sitter-elisp" "master" "src")
               (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src"))
               (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
      (add-to-list 'treesit-language-source-alist grammar)
      ;; Only install `grammar' if we don't already have it
      ;; installed. However, if you want to *update* a grammar then
      ;; this obviously prevents that from happening.
      (unless (treesit-language-available-p (car grammar))
        (treesit-install-language-grammar (car grammar)))))

  ;; Optional, but recommended. Tree-sitter enabled major modes are
  ;; distinct from their ordinary counterparts.
  ;;
  ;; You can remap major modes with `major-mode-remap-alist'. Note
  ;; that this does *not* extend to hooks! Make sure you migrate them
  ;; also
  (dolist (mapping '((python-mode . python-ts-mode)
                     (css-mode . css-ts-mode)
                     (typescript-mode . tsx-ts-mode)
                     (js-mode . js-ts-mode)
                     (java-mode . java-ts-mode)
                     (css-mode . css-ts-mode)
                     (yaml-mode . yaml-ts-mode)))
    (add-to-list 'major-mode-remap-alist mapping))

  :config
  (mp-setup-install-grammars)
  ;; Do not forget to customize Combobulate to your liking:
  ;;
  ;;  M-x customize-group RET combobulate RET
  ;;
  (eval `(use-package combobulate
           :preface
           ;; You can customize Combobulate's key prefix here.
           ;; Note that you may have to restart Emacs for this to take effect!
           (setq combobulate-key-prefix "C-c C-o")

           ;; Optional, but recommended.
           ;;
           ;; You can manually enable Combobulate with `M-x
           ;; combobulate-mode'.
           :hook ((python-ts-mode . combobulate-mode)
                  (js-ts-mode . combobulate-mode)
                  (css-ts-mode . combobulate-mode)
                  (yaml-ts-mode . combobulate-mode)
                  (typescript-ts-mode . combobulate-mode)
                  (tsx-ts-mode . combobulate-mode))
           ;; Amend this to the directory where you keep Combobulate's source
           ;; code.
           :load-path ,(expand-file-name "combobulate" init-library-path)))))
