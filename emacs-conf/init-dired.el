;;; init-dired.el --- Dired configuration

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'wdired-change-to-wdired-mode "wdired" t)

(defun init-dired-load ()
  "Configurations which should be loaded with dired."
  (load "dired-x")
  (define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
  (define-key dired-mode-map "b"
    (lambda ()
      (interactive)
      (setq init-project-root (dired-current-directory))
      (message "Project root: %s" (dired-current-directory)))))

;; dired may have already been loaded, in which case `dired-load-hook'
;; will not be useful.
(if (member 'dired features)
    (init-dired-load)
  (add-hook 'dired-load-hook
	    'init-dired-load))

(define-key global-map "\C-x\C-j" 'dired-jump)

(require 'key-chord)
(add-hook 'dired-mode-hook (lambda ()
                             (key-chord-define dired-mode-map ".."
                                               'dired-up-directory)))

(setq dired-recursive-copies 'top
      dired-recursive-deletes 'top
      dired-omit-files "^\\.[^.].+"
      dired-guess-shell-alist-user '(("\\.pdf\\'" "evince")))

(put 'dired-find-alternate-file 'disabled nil)

(init-alias-new "home" (lambda () (interactive) (dired (getenv "HOME"))))

;;; init-dired.el ends here
