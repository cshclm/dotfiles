;; init-misc.el -- Miscellaneous configuration settings for Emacs.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "misc" init-library-path))

(setq-default fill-column 78)
(setq-default truncate-lines nil)
(setq-default indent-tabs-mode nil)
(setq-default case-fold-search t)
(setq initial-buffer-choice t
      initial-scratch-message nil
      require-final-newline nil
      display-buffer-reuse-frames nil
      enable-recursive-minibuffers t
      confirm-nonexistent-file-or-buffer nil
      default-major-mode 'text-mode
      case-fold-search t
      line-move-visual nil
      completion-show-help nil
      initial-buffer-choice nil
      use-dialog-box nil
      inhibit-startup-screen t
      comint-use-prompt-regexp t
      current-language-environment "Latin-1"
      default-input-method "latin-1-prefix"
      display-time-24hr-format t
      isearch-lax-whitespace nil
      woman-use-own-frame nil
      glasses-separator ""
      glasses-original-separator ""
      vc-follow-symlinks t
      send-mail-function 'smtpmail-send-it
      sendmail-program "/usr/bin/msmtp"
      mail-specify-envelope-from t
      message-sendmail-envelope-from 'header
      mail-envelope-from 'header
      send-mail-function 'message-send-mail-with-sendmail
      time-stamp-active t
      transient-mark-mode t
      set-mark-command-repeat-pop t
      truncate-partial-width-windows 0
      abbrev-file-name (expand-file-name "abbrevs" user-emacs-directory)
      time-stamp-format "%:y-%02m-%02dT%02H:%02M:%02S%5z"
      Info-additional-directory-list '("~/doc/info/")
      whitespace-style '(face trailing space-before-tab indentation empty space-after-tab)
      dabbrev-case-distinction nil
      dabbrev-case-fold-search nil
      plstore-cache-passphrase-for-symmetric-encryption t)
(blink-cursor-mode -1)
(and (fboundp 'minibuffer-depth-indicate-mode)
     (minibuffer-depth-indicate-mode 1))
(column-number-mode 1)
(global-hi-lock-mode t)
(electric-indent-mode t)
(defalias 'yes-or-no-p 'y-or-n-p)
(add-hook 'after-save-hook
	  'executable-make-buffer-file-executable-if-script-p)
(add-hook 'before-save-hook 'time-stamp)
(add-hook 'text-mode-hook
	  #'(lambda ()
	      (setq indent-tabs-mode nil)))
(set-default 'abbrev-mode t)
(set-default 'show-trailing-whitespace nil)
(when (file-exists-p abbrev-file-name) (read-abbrev-file))

(put 'narrow-to-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'scroll-left 'disabled nil)

(load (expand-file-name "ssh-agent" init-library-path))
;; I can't seem to get used to the `ssh-agent' command...
(defalias 'ssh-add 'ssh-agent)
(setq ssh-add-args "/home/cmann/.ssh/id_rsa.atlassian"
      warning-minimum-log-level :error)

(defun init-ssh-add ()
  (interactive)
  (mapcar (lambda (f) (let ((ssh-add-args f))
                        (message "%s" ssh-add-args)
                        (ssh-add)))
          init-ssh-keys))

(defvar init-read-only-buffers '()
  "Buffers to be set read-only.")

(defun init-default-read-only ()
  "Mark files as read-only based on buffer name.
The list of buffers to be marked read-only is defined in
`init-read-only-buffers'."
  (when (member (buffer-name) init-read-only-buffers)
    (read-only-mode 1)))

(add-hook 'find-file-hook 'init-default-read-only)

(autoload 'outline-cycle "outline-magic"
  "Visibility cycling for outline(-minor)-mode.")

(autoload 'multiple-cursors "multiple-cursors"
  "Provide editing with muliple cursors" t)
(add-to-list 'load-path (expand-file-name "arduino-mode" init-library-path))
(autoload 'arduino-mode "arduino-mode"
  "Major mode for editing Arduino code." t)
(add-to-list 'auto-mode-alist '("\\.ino\\'" . arduino-mode))

(autoload 'yaml-mode "yaml-mode" "Mode for editing YAML files." t)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))

(avy-setup-default)
(global-set-key (kbd "C-c s") #'avy-goto-word-1)
(global-set-key (kbd "C-x j") #'duplicate-dwim)

(require 'winner)
(winner-mode 1)

(add-to-list 'load-path (expand-file-name "auth-source-xoauth2" init-library-path))
(require 'auth-source-xoauth2)
(auth-source-xoauth2-enable)

(require 'edit-server)
(setq edit-server-new-frame nil)
(edit-server-start)

(require 'paren)
(set-face-background 'show-paren-match "darkgreen")
(set-face-background 'show-paren-mismatch "red4")
(show-paren-mode 1)


;;; init-misc.el ends here
