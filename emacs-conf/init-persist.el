;;; init-persist.el --- Items which persist across Emacs sessions

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(unless (> emacs-major-version 21)
  (error "%s" "Minimum supported version is Emacs 22"))

(savehist-mode t)
(add-to-list 'backup-directory-alist
	     (cons "." (expand-file-name "backup" user-emacs-directory)))
(add-to-list 'backup-directory-alist
	     (cons tramp-file-name-regexp nil))
(setq backup-by-copying t
      desktop-buffers-not-to-save
      "\\(^nn\\.a[0-9]+\\|\\.log\\|(ftp)\\|Map_Sym.txt\\|diary\\|.gpg\\|\\*Music\\*\\)$"
      desktop-load-locked-desktop nil
      desktop-path (list (expand-file-name "~"))
      desktop-dirname (getenv "HOME"))

(add-hook 'desktop-not-loaded-hook #'(lambda () (desktop-save-mode 0)))

(desktop-save-mode t)

;;; init-persist.el ends here
