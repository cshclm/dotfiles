;;; init-tex.el --- Auctex configuration

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'tex)

(setq TeX-auto-save t
      TeX-parse-self t
      TeX-auto-local ".auto"
      tex-dvi-view-command "/usr/bin/xdvi"
      TeX-view-program-list '(("Evince" "evince %o"))
      TeX-view-program-selection
      '(((output-dvi style-pstricks) "dvips and gv")
        (output-dvi "xdvi")
        (output-pdf "Evince")
        (output-html "xdg-open")))

(setq-default TeX-master nil)

 (defun init-determine-template ()
  "Attempt to determine possible skeletons to insert into the current buffer."
  (when buffer-file-name
    (let ((result t))
      (save-excursion
        (forward-line 1)
        (when (looking-at "^%%% Local Variables:$")
          (setq result nil)))
      (when result
        (cond
         ((string-match "letters/[^/]+\\.tex$\\|-letter\\.tex$" buffer-file-name)
          (latex-letter-skeleton)))))))

(add-hook 'LaTeX-mode-hook 'init-determine-template)

;;; init-tex.el ends here
