;;; init-bbdb.el --- Configuration for Big Brother Database

;; Copyright (C) 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "bbdb/lisp" init-library-path))
(autoload 'bbdb "bbdb" "Big Brother Database" t)

(setq bbdb-file (expand-file-name "bbdb" user-emacs-directory)
      bbdb-north-american-phone-numbers-p nil
      bbdb-check-zip-codes nil
      bbdb-complete-name-allow-cycling t
      bbdb-use-pop-up nil)

(defun init-initialise-bbdb ()
  "Prepare BBDB for use."
  (require 'bbdb)
  (bbdb-initialize 'gnus 'message 'mail)
  (bbdb-insinuate-gnus)
  (bbdb-insinuate-message)
  (bbdb-mail-aliases)
  (init-alias-new "contact" 'bbdb)
  (setq bbdb-complete-mail-allow-cycling t
      bbdb-mua-pop-up nil)
  (bbdb-mua-auto-update-init 'gnus 'message 'mail)
  (and (init-depends "keybindings")
       (global-set-key (kbd "C-c f") 'bbdb)))



;;; init-bbdb.el ends here
