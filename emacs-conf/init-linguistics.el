;;; init-linguistics.el --- Configuration for dictionary, thesaurus, etc.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "dictionary" init-library-path))

(autoload 'dictionary-search "dictionary"
  "Search for a word in the dictionary" t)

(global-set-key (kbd "C-c d D") 'init-dictionary-search)

(setq dictionary-use-single-buffer t)

;; Modification to `dictionary-search' to prevent automatic insertion of the
;; element at point.  The behaviour is now more like `dict', in that in
;; provides a default entry instead of automatically inserting `current-word'.
(defun init-dictionary-search (word &optional dictionary)
  "Search the `word' in `dictionary' if given or in all if nil.
It presents the word at point as default input and allows editing it."
  (interactive
   (list (let* ((default-entry (current-word nil t))
                (input (read-string
                        (format "Dict entry%s: "
                                (if (not default-entry)
                                    ""
                                  (format " (default %s)" default-entry))))))
           (if (string= input "")
               (if (string= default-entry "")
                   (error "No dict args given") default-entry) input))
	 (if current-prefix-arg
	     (read-string "Dictionary: "))))
  ;; Just in case non-interactively called
  (dictionary-search word dictionary)
  (connection-close dictionary-connection))

(autoload 'synonyms "synonyms"
  "Show synonyms that match a regular expression" t)
(setq synonyms-file (expand-file-name "mthesaur.txt" user-emacs-directory)
      synonyms-cache-file (expand-file-name "mthesaur.txt.cache"
					    user-emacs-directory))

(eval-after-load "synonyms"
  '(progn
     (set-face-foreground 'synonyms-heading "unspecified")
     (set-face-foreground 'synonyms-link "unspecified")
     (set-face-foreground 'synonyms-search-text "cyan")))

(setq ispell-choices-win-default-height 3)

;;; init-linguistics.el ends here
