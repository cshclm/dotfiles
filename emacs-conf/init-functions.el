;;; init-functions.el --- Miscellaneous functions and extensions

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defun init-move-beginning-of-line (arg)
  "Move to the next BoL position.
If point is at BoL, move to the beginning of the previous line.
Otherwise, move to the beginning of the current line.
When ARG, move backward ARG lines."
  (interactive "P")
  (when (bobp)
    (error "Beginning of buffer"))
  (if (and arg (numberp arg))
      (forward-line (* -1 arg))
    (if (bolp)
	(forward-line -1)
      (beginning-of-line))))

(defun init-move-end-of-line (arg)
  "Move to the next EoL position.
If point is at EoL, move to the end of the next line.
Otherwise, move to the end of the current line.
When ARG, move forward ARG lines."
  (interactive "P")
  (when (eobp)
    (error "End of buffer"))
  (if (and arg (numberp arg))
      (progn
	(forward-line arg)
	(end-of-line))
    (if (eolp)
	(progn
	  (forward-line 1)
	  (end-of-line))
      (end-of-line))))

(defun init-pretty-print-eval (&optional delete-sexp)
  "Evaluate and insert the result where possible.
Evaluate the preceeding sexp as normal.  If provided a prefix
argument, and currently editing a command, insert the result."
  (interactive "P")
  (let ((res (format "%s" (eval (preceding-sexp))))
        (opoint (point)))
    (if (not delete-sexp)
        (insert " => " res)
      (forward-sexp -1)
      (delete-region opoint (point))
      (insert res))))

(defun count-matching-lines (regexp)
  "Count the number of lines which match REGEXP."
  (interactive "sRegexp: ")
  (let ((count 0))
    (save-excursion
      (while (search-forward-regexp string nil t)
	(setq count (1+ count))
	(forward-line)
	(beginning-of-line)))
    (message "%d matches" count)))

(defun init-string-remove-trailing-character (char string)
  "Remove CHAR from the end of STRING."
  (let ((l (nreverse (string-to-list string))))
    (while (= (car l) char)
      (setq l (cdr l)))
    (concat (nreverse l))))

(defun init-escape-string (string)
  "Escape common problematic characters in STRING."
  (replace-regexp-in-string
   "[\n\t\"\\]" (lambda (x) (concat "\\\\" x)) string))

(defun init-buffers-with-active-process ()
  "Return a list of buffers with active processes."
  (let ((processes (process-list))
        results)
    (while processes
      (let ((pbuf (process-buffer (car processes))))
        (when pbuf
          (setq results (cons (buffer-name pbuf) results))))
      (setq processes (cdr processes)))
    results))

(defun init-buffers-with-mode (&rest modes)
  "Return a list of buffers with active processes."
  (let ((buffers (buffer-list))
        results)
    (while buffers
      (when (member (save-excursion (set-buffer (car buffers)) major-mode)
                    modes)
        (setq results (cons (buffer-name (car buffers)) results)))
      (setq buffers (cdr buffers)))
    results))

(defun init-clean-buffers ()
  "Kill buffers which are no longer needed."
  (interactive)
  (let ((buffers (buffer-list))
        (keep-buffers (append '("*scratch*" "*Messages*" "*Music*" "*Group*"
                                "*Org Agenda*" "&bitlbee")
                              (init-buffers-with-active-process)
                              (init-buffers-with-mode 'rcirc-mode 'org-mode))))
    (while buffers
      (when (and (or (and (not (buffer-modified-p (car buffers)))
                          (buffer-file-name (car buffers)))
                     (not (buffer-file-name (car buffers))))
                 (not (member (buffer-name (car buffers)) keep-buffers))
                 ;; buffers can apparently have no name?
                 (not (and (buffer-name (car buffers))
                           (string-match "^\\ "
                                         (buffer-name (car buffers))))))
        (kill-buffer (car buffers)))
      (setq buffers (cdr buffers)))))

(defun init-get-hostname ()
  "Get the hostname of the current system."
  (when (eq system-type 'gnu/linux)
    (save-excursion
      (with-temp-buffer
	(shell-command "hostname" t)
	(when (looking-at "[a-zA-Z0-9_.]+")
	  (match-string 0))))))

(defun search-files-recursively (path regexp)
  "Return files matching REGEXP within PATH."
  (let ((files (directory-files path t))
        results)
    (while files
      (cond
       ;; Ignore filenames beginning with '.' and symlinks.
       ((or (string-match "^\\." (file-name-nondirectory (car files)))
            (file-symlink-p (car files))))
       ;; File is a directory; check it.
       ((file-directory-p (car files))
        (setq results
              (append results (search-files-recursively (car files) regexp))))
       ;; Add the file to the results; it's what we're looking for.
       ((string-match regexp (car files))
        (add-to-list 'results (car files))))
      (setq files (cdr files)))
    results))

(defun init-buffer-exists-p (buffer)
  "Return non-nil if a buffer named BUFFER exists.
Otherwise, return nil."
  (let ((buffers (buffer-list)))
    (while (and buffers
		(not (string= buffer (buffer-name (car buffers)))))
      (setq buffers (cdr buffers)))
    (not (null (car buffers)))))

;; Advise the odd implementation of `display-startup-echo-area-message' to
;; behave in a more sane manner.  This enables the configuration to silence
;; the startup message and show the most recent error, regardless of the
;; username, if problems are encountered on initialisation.
(defadvice display-startup-echo-area-message (around
					      force-inhibit-startup-message
					      first nil activate)
  "Provide the means to prevent startup message regardless of login name.
Setting `inhibit-startup-echo-area-message' to
`user-login-name' (or equivalent) will prevent the startup
message from being displayed.
`inhibit-startup-echo-area-message' should be assigned to an
explicit login name when it is not crucial to disable
`startup-echo-area-message'."
  (or (and (not noninteractive)
	   (string= inhibit-startup-echo-area-message user-login-name))
      ad-do-it))

(defun init-command-on-buffer (command buffername)
  "Display the results of COMMAND on the current buffer.

BUFFERNAME is the name to be given to the output buffer."
  (interactive)
  (let ((perlbuf (current-buffer))
        (checkfile (make-temp-file "cob"))
        (checkbuf (get-buffer-create buffername))
        (map (make-sparse-keymap)))
    (with-temp-file checkfile
      (insert-buffer-substring perlbuf))
    (set-buffer checkbuf)
    (save-excursion
      (define-key map (kbd "q") 'bury-buffer)
      (use-local-map map)
      (display-buffer checkbuf)
      (if (eq 0 (shell-command (concat command " " checkfile) checkbuf))
          (progn
            (message (replace-regexp-in-string "\n" "" (buffer-string)))
            (delete-window (get-buffer-window checkbuf)))
        (setq buffer-read-only t)))
    (delete-file checkfile)))

(defvar init-project-root ""
  "Root directory for the current project.")

(defun init-command-on-project (command buffername)
  "Display the results of COMMAND at `init-project-root'.

BUFFERNAME is the name to be given to the output buffer."
  (interactive)
  (let ((perlbuf (current-buffer))
        (checkbuf (get-buffer-create buffername))
        (map (make-sparse-keymap)))
    (set-buffer checkbuf)
    (use-local-map map)
    (save-excursion
      (let ((default-directory init-project-root))
        (define-key map (kbd "q") 'bury-buffer)
        (shell-command (concat command) checkbuf)))
    (display-buffer checkbuf)
    (setq buffer-read-only t)))

(defun init-alias-map (map)
  "Create aliases given a list of symbols and definitions.

MAP is a list of the form ((ALIAS . SYMBOL) ...)"
  (mapc (lambda (x) (init-alias-new (car x) (cdr x))) map))

(defun init-alias-new (alias symbol)
  "Create an alias named '@ALIAS' to SYMBOL."
  (defalias (intern (concat "@" alias)) symbol))

(defun init-read-file (filename)
  "Return the contents of FILENAME as a string."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(defun init-read-lines (filename)
  "Return the contents of FILENAME as a list of strings."
  (split-string (init-read-file filename) "\n" t))

(defun init-list-occurrences (regexp)
  "Return a list of all matching occurrences.  Use the first match group
 if one is available in regexp.  Otherwise, use the full matching
 string."
  (save-excursion
    (let ((acc '()))
      (while (not (eobp))
        (search-forward-regexp regexp (point-max) 'end)
        (add-to-list 'acc (or (match-string-no-properties 1) (match-string-no-properties 0))))
      (reverse acc))))

(defun init-list-occurrences-in-file (file regexp)
  (save-window-excursion
    (find-file file)
    ;; TODO consider narrowing
    (beginning-of-buffer)
    (init-list-occurrences regexp)))

;;; init-functions.el


