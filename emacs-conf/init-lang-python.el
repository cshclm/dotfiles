;;; init-pythonig.el -- Configure python

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defcustom init-pylint-strict t
  "Whether pylint should check the code strictly."
  :type 'boolean
  :group 'init)

(defcustom init-python-use-flymake nil
  "Whether flymake should be enabled in python-mode buffers."
  :type 'boolean
  :group 'init)

(defvar init-flymake-pylint-timer nil
  "Timer for flymake error display")

(autoload 'python-switch-to-python "python"
  "Switch to the Python process buffer." t)

(unless (fboundp 'init-flymake-display-line)
  (defun init-flymake-display-line()
    "Display error/warnings as Emacs messages in the minibuffer"
    (interactive)
    (let ((err-text
	   (flymake-ler-text
	    (caar (flymake-find-err-info
		   flymake-err-info (flymake-current-line-no))))))
      (when err-text
	(message "%s" err-text)))))

(eval-after-load "python"
  '(progn
     (require 'flymake)
     (add-to-list 'flymake-allowed-file-name-masks
		  '("\\.py\\'" init-flymake-pylint-init))
     (define-key python-mode-map "\C-ch" 'init-python-documentation)
     (set-face-background 'flymake-errline "red4")
     (set-face-background 'flymake-warnline "midnightblue")))

(defun init-python-pylint-buffer ()
  "Run pylint on the current buffer.
Create a temporary file if necessary."
  (interactive)
  (let ((check-file-name buffer-file-name)
	(temp-file nil))
    (when (or (not check-file-name) (buffer-modified-p))
      (setq check-file-name (make-temp-file "Pylint")
	    temp-file t)
      (write-region (point-min) (point-max) check-file-name))
    (shell-command (format "pylint %s" check-file-name))
    (and temp-file
	 (delete-file check-file-name))))

(defun init-flymake-pylint-init ()
  "Initialise flymake using pylint on the current buffer."
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
		     'flymake-create-temp-inplace))
	 (local-file (file-relative-name
		      temp-file
		      (file-name-directory buffer-file-name)))
	 (strict (if init-pylint-strict "strict" "nil")))
    (list "epylint.py" (list strict local-file))))

(defun init-python-documentation (string)
  "Launch PyDOC on the word at Point"
  (interactive
   (list
    (let* ((word (thing-at-point 'symbol))
	   (input
	    (read-string
	     (format "PyDoc entry%s: "
		     (if (not word)
			 "" (format " (default %s)" word))))))
      (if (string= input "")
	  (if (not word)
	      (error "PyDoc arguments not given")
	    word)
	input))))
  (shell-command
   (concat python-command
	   " -c \"from pydoc import help;help(\'" string "\')\"") "*PyDoc*")
  (view-buffer-other-window "*PyDoc*" t 'kill-buffer-and-window))

(add-hook 'python-mode-hook
	  #'(lambda ()
              (when init-python-use-flymake
                (flymake-mode 1))
              (flycheck-mode 1)
	      (define-key python-mode-map (kbd "C-c C-M-v")
	        'init-python-pylint-buffer)))

(setq flymake-gui-warnings-enabled nil
      python-check-command "pyflakes3k")

;;; init-python.el ends here
