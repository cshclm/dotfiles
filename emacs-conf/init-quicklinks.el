;; init-quicklinks.el --- Quick links

;; Copyright (C) 2021 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defvar init-org-quicklinks-cmd-tag "quickcmd")
(defvar init-org-quicklinks-link-tag "quicklink")
(defvar init-org-quicklinks-default-link-command "xdg-open")

(defun init-org-quicklinks--extract-property (hl property)
  (plist-get (cadr hl) property))

(defun init-org-quicklinks--quicklinks-tag-p (tag)
  (member tag (list init-org-quicklinks-cmd-tag init-org-quicklinks-link-tag)))


(defun init-org-quicklinks--extract-quicklink-properties (hl)
  (cons (substring-no-properties (car (remove-if #'init-org-quicklinks--quicklinks-tag-p
                                                 (init-org-quicklinks--extract-property hl :tags))))
        (concat init-org-quicklinks-default-link-command " " (init-org-quicklinks--extract-property hl :raw-value))))

(defun init-org-quicklinks--extract-quickcmd-properties (hl)
  (cons (substring-no-properties (car (remove-if #'init-org-quicklinks--quicklinks-tag-p
                                                 (init-org-quicklinks--extract-property hl :tags))))
        (init-org-quicklinks--extract-property hl :raw-value)))

(defun init-org-quicklinks--all-tag-url-pairs ()
  (append (mapcar #'init-org-quicklinks--extract-quicklink-properties
                  (org-ql-query :select 'element :from (org-agenda-files) :where (quote (tags init-org-quicklinks-link-tag))))
          (mapcar #'init-org-quicklinks--extract-quickcmd-properties
                  (org-ql-query :select 'element :from (org-agenda-files) :where (quote (tags init-org-quicklinks-cmd-tag))))))

(defun init-org-quicklinks-search-quicklinks (prefix &optional as-string)
  (let ((results (sort (mapcar #'car
                              (cl-remove-if (lambda (tag-to-url)
                                              (let ((key (car tag-to-url)))
                                                (not (string-prefix-p prefix key))))
                                            (init-org-quicklinks--all-tag-url-pairs)))
                      #'string<)))
    (if as-string
        (string-join results "\n")
      results)))

(defun init-org-quicklinks-find-quicklink (prefix)
  (cdr (cl-find-if (lambda (tag-to-url)
                     (let ((key (car tag-to-url)))
                       (string= prefix key)))
                   (init-org-quicklinks--all-tag-url-pairs))))

;;; init-quicklinks.el ends here
