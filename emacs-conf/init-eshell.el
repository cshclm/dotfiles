;;; init-eshell.el --- Configuration for eshell

;; Copyright (C) 2023 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'eshell)
(require 'em-smart)
(setq eshell-where-to-jump 'begin)
(setq eshell-review-quick-commands nil)
(setq eshell-smart-space-goes-to-end t)
(add-to-list 'eshell-modules-list 'eshell-smart)

(when (init-depends "keybindings")
  (global-set-key (kbd "C-c t") 'eshell))


(defun eshell/do (&rest args)
  "Execute a command sequence over a collection of file elements.
Separate the sequence and the elements with a `::' string.
For instance:

    do chown _ angela :: *.org(u'oscar')

The function substitutes the `_' sequence to a single filename
element, and if not specified, it appends the file name to the
command. So the following works as expected:

    do chmod a+x :: *.org"
  (seq-let (forms elements) (-split-on "::" args)
    (mapcar (lambda (element)
              (message "Working on %s ... %s" element forms)
              (let* ((form (if (-contains? forms "_")
                               (-replace "_" element forms)
                             (-snoc forms element)))
                     (cmd  (car form))
                     (args (cdr form)))
                (message "%s %s" cmd args)
                (eshell-named-command cmd args)))
            (-flatten (-concat elements)))))

(defun eshell/output (&rest args)
  "Return an eshell command output from its history.

The first argument is the index into the historical past, where
`0' is the most recent, `1' is the next oldest, etc.

The second argument represents the returned output:
 * `text' :: as a string
 * `list' :: as a list of elements separated by whitespace
 * `file' :: as a filename that contains the output

If the first argument is not a number, it assumes the format
to be `:text'.
"
  (let (frmt element)
    (cond
     ((> (length args) 1)  (setq frmt (cadr args)
                                 element (car args)))
     ((= (length args) 0)  (setq frmt "text"
                                 element 0))
     ((numberp (car args)) (setq frmt "text"
                                 element (car args)))
     ((= (length args) 1)  (setq frmt (car args)
                                 element 0)))

    (if-let ((results (ring-ref ha-eshell-output (or element 0))))
        (cl-case (string-to-char frmt)
          (?l     (split-string results))
          (?f     (ha-eshell-store-file-output results))
          (otherwise (s-trim results)))
      "")))

(defun ha-eshell-store-file-output (results)
  "Writes the string, RESULTS, to a temporary file and returns that file name."
  (let ((filename (make-temp-file "ha-eshell-")))
    (with-temp-file filename
      (insert results))
    filename))

(defun ha-eshell-engineering-notebook (capture-template args)
  "Capture commands and output from Eshell into an Engineering Notebook.

Usage:   ex [ options ] [ command string ] [ :: prefixed comments ]]

A _command string_ is an eshell-compatible shell comman to run,
and if not given, uses previous commands in the Eshell history.

Options:
  -c, --comment   A comment string displayed before the command
  -n, --history   The historical command to use, where `0' is the
                  previous command, and `1' is the command before that.
  -t, --template  The `keys' string to specify the capture template"
  (let* (output
         (options  (eshell-getopts
                    '((:name comment :short "c" :long "comment" :parameter string)
                      (:name history :short "n" :long "history" :parameter integer)
                      (:name captemp :short "t" :long "template" :parameter string)
                      (:name interact :short "i" :long "interactive")
                      (:name help    :short "h" :long "help"
                             :help ha-eshell-engineering-notebook))
                    args))
         (sh-call  (gethash 'parameters options))
         (sh-parts (-split-on "::" sh-call))
         (command  (s-join " " (first sh-parts)))
         ;; Combine the -c parameter with text following ::
         (comment  (s-join " " (cons (gethash 'comment options)
                                     (second sh-parts))))
         (history  (or (gethash 'history options) 0)))

    ;; Given a -t option? Override the default:
    (when (gethash 'captemp options)
      (setq capture-template (gethash 'captemp options)))

    (when (gethash 'interact options)
      (setq capture-template "ee"))

    (cond
     (sh-call   ; Gotta a command, run it!
      (ha-eshell-engineering-capture capture-template comment command
                                     (eshell-command-to-string (first sh-parts))))
     (t         ; Otherwise, get the history
      (ha-eshell-engineering-capture capture-template comment
                                     (ring-ref eshell-history-ring (1+ history))
                                     (eshell/output history))))))

(defun ha-eshell-engineering-capture (capture-template comment cmd out)
  "Capture formatted string in CAPTURE-TEMPLATE.
Base the string created on COMMENT, CMD, and OUT. Return OUTPUT."
  (let* ((command (when cmd (s-trim cmd)))
         (output  (when out (s-trim out)))
         (results (concat
                   (when comment (format "%s\n\n" comment))
                   (when command (format "#+begin_src shell\n  %s\n#+end_src\n\n" command))
                   (when (and command output) "#+results:\n")
                   (when output  (format "#+begin_example\n%s\n#+end_example\n" output)))))

    (message results)
    (org-capture-string results capture-template)

    ;; Return output from the command, or nothing if there wasn't anything:
    (or output "")))

(defun eshell/en (&rest args)
  "Call `ha-eshell-engineering-notebook' to \"General Notes\"."
  (interactive)
  (ha-eshell-engineering-notebook "ef" args))

(defun eshell/ec (&rest args)
  "Call `ha-eshell-engineering-notebook' to current clocked-in task."
  (interactive)
  (ha-eshell-engineering-notebook "cc" args))

(defun eshell/cap (&rest args)
  "Call `org-capture' with the `ee' template to enter text into the engineering notebook."
  (org-capture nil "ee"))

(defun ha-eshell-target-engineering-notebook (output)
  "Write OUTPUT into the engineering notebook via `org-capture'."
  (ha-eshell-engineering-capture "ef" nil nil output))

(defun ha-eshell-target-clocked-in-task (output)
  "Write OUTPUT into the current clocked in task via `org-capture'."
  (ha-eshell-engineering-capture "cc" nil nil output))

(with-eval-after-load "eshell"
  (add-to-list 'eshell-virtual-targets '("/dev/e" ha-eshell-target-engineering-notebook nil)))

(defun curr-dir-git-branch-string (pwd)
  "Returns current git branch as a string, or the empty string if
PWD is not in a git repo (or the git command is not found)."
  (interactive)
  (when (and (not (file-remote-p pwd))
             (eshell-search-path "git")
             (locate-dominating-file pwd ".git"))
    (let* ((git-url    (shell-command-to-string "git config --get remote.origin.url"))
           (git-repo   (file-name-base (s-trim git-url)))
           (git-output (shell-command-to-string (concat "git rev-parse --abbrev-ref HEAD")))
           (git-branch (s-trim git-output))
           (git-icon   "\xe0a0")
           (git-icon2  (propertize "/" 'face `(:family "octicons"))))
      (concat git-repo " " git-icon2 " " git-branch))))

(defun pwd-replace-home (pwd)
  "Replace home in PWD with tilde (~) character."
  (interactive)
  (let* ((home (expand-file-name (getenv "HOME")))
         (home-len (length home)))
    (if (and
         (>= (length pwd) home-len)
         (equal home (substring pwd 0 home-len)))
        (concat "~" (substring pwd home-len))
      pwd)))

(defun pwd-shorten-dirs (pwd)
  "Shorten all directory names in PWD except the last two."
  (let ((p-lst (split-string pwd "/")))
    (if (> (length p-lst) 2)
        (concat
         (mapconcat (lambda (elm) (if (zerop (length elm)) ""
                               (substring elm 0 1)))
                    (butlast p-lst 2)
                    "/")
         "/"
         (mapconcat (lambda (elm) elm)
                    (last p-lst 2)
                    "/"))
      pwd)))  ;; Otherwise, we return the PWD

(defun split-directory-prompt (directory)
  (if (string-match-p ".*/.*" directory)
      (list (file-name-directory directory) (file-name-base directory))
    (list "" directory)))

(defun eshell/eshell-local-prompt-function ()
  "A prompt for eshell that works locally (in that it assumes it
could run certain commands) to make a prettier, more-helpful
local prompt."
  (interactive)
  (let* ((pwd        (eshell/pwd))
         (directory (split-directory-prompt
                     (pwd-shorten-dirs
                      (pwd-replace-home pwd))))
         (parent (car directory))
         (name   (cadr directory))
         (branch (curr-dir-git-branch-string pwd))

         (dark-env (eq 'dark (frame-parameter nil 'background-mode)))
         (for-bars                 `(:weight bold))
         (for-parent  (if dark-env `(:foreground "dark orange") `(:foreground "blue")))
         (for-dir     (if dark-env `(:foreground "orange" :weight bold)
                        `(:foreground "blue" :weight bold)))
         (for-git                  `(:foreground "green"))
         (for-ruby                 `(:foreground "red"))
         (for-python               `(:foreground "#5555FF")))

    (concat
     (propertize "⟣─ "    'face for-bars)
     (propertize parent   'face for-parent)
     (propertize name     'face for-dir)
     (when branch
       (concat (propertize " ── "    'face for-bars)
               (propertize branch   'face for-git)))
     ;; (when ruby
     ;;   (concat (propertize " ── " 'face for-bars)
     ;;           (propertize ruby   'face for-ruby)))
     ;; (when python
     ;;   (concat (propertize " ── " 'face for-bars)
     ;;           (propertize python 'face for-python)))
     (propertize "\n"     'face for-bars)
     (propertize (if (= (user-uid) 0) " #" " $") 'face `(:weight ultra-bold))
     ;; (propertize " └→" 'face (if (= (user-uid) 0) `(:weight ultra-bold :foreground "red") `(:weight ultra-bold)))
     (propertize " "    'face `(:weight bold)))))

(setq-default eshell-prompt-function #'eshell/eshell-local-prompt-function)

(add-hook 'eshell-mode-hook 'eshell-fringe-status-mode)

(eshell-syntax-highlighting-global-mode)
