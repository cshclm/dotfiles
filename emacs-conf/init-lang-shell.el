;;; init-lang-shell.el --- Configuration for shell handling

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(setq comint-scroll-to-bottom-on-input t
      comint-scroll-to-bottom-on-output t
      comint-scroll-show-maximum-output t
      comint-completion-autolist t
      comint-input-ignoredups t
      comint-completion-addsuffix t)

(ansi-color-for-comint-mode-on)

(when (init-depends "keybindings")
  (global-set-key (kbd "C-c T") 'init-ansi-term))

(defadvice term-handle-exit (after term-kill-buffer-when-done
                                   first nil activate)
  "Remove the term buffer once the process ends."
  (if (cdr (window-list))
      (kill-buffer-and-window)
    (kill-buffer)))

(defun init-ansi-term ()
  "Create an `ansi-term' in a term window."
  (interactive)
  (split-window-sensibly (get-buffer-window (current-buffer)))
  (ansi-term (getenv "SHELL")))

(defun init-shell-active-prompt-p ()
  "Determine whether the active prompt is on the current line.
Return non-nil if an active prompt is on the current line,
otherwise return nil."
  (save-excursion
    (beginning-of-line)
    (and (looking-at comint-prompt-regexp)
	 (= (line-number-at-pos (point))
	    (line-number-at-pos (point-max))))))

(defun init-shell-bol-dwim ()
  "Move point to the beginning of the line or start of prompt."
  (interactive)
  (if (init-shell-active-prompt-p)
      (comint-bol)
    (beginning-of-line)))

(defun init-shell-toggle-sudo ()
  "Toggle sudo prefix of the current command."
  (interactive)
  (save-excursion
    (when (init-shell-active-prompt-p)
      (comint-bol)
      (if (looking-at "sudo ")
	  (delete-char (length "sudo "))
	(insert "sudo ")))))

(defun init-shell-clear-or-recenter (&optional arg)
  "Clear the screen or call `recenter-top-bottom'.
If point is on the active prompt, clear the screen, otherwise
call `recenter-top-bottom'."
  (interactive "p")
  (save-excursion
    (beginning-of-line)
    (if (init-shell-active-prompt-p)
	(recenter-top-bottom 0)
      (recenter-top-bottom))))

(add-hook 'shell-mode-hook
	  #'(lambda ()
              (define-key shell-mode-map "\C-l" 'init-shell-clear-or-recenter)
	      (define-key shell-mode-map "\C-cs" 'init-shell-toggle-sudo)
	      (define-key shell-mode-map "\C-a" 'init-shell-bol-dwim)))

;;; init-shell.el ends here
