;;; init-tramp.el --- Tramp configuration

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defun init-tramp-guess-proxies ()
  "Configure tramp proxies for other hosts on the network."
  (when (boundp 'personal-network-hosts)
    (let ((hosts (remove (init-get-hostname) personal-network-hosts))
          result)
      (dolist (host hosts)
        (setq result
              (cons (list host "\\`root\\'" "/ssh:%h:") result)))
      (setq tramp-default-proxies-alist result))))

(init-tramp-guess-proxies)

(setq tramp-shell-prompt-pattern "^.*[#$%>] *")

;;; init-tramp.el ends here
