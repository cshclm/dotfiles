;;; init-lisp.el --- Configuration to handle editing of Lisp variants.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "misc" init-library-path))

(autoload 'paredit-mode "paredit"
  "Pseudo-structural editing for Lisp code." t)

(autoload 'stumpwm-mode "stumpwm-mode"
  "Evaluate StumpWM expressions using stumpish." t)

(add-hook 'lisp-mode-hook
	  #'(lambda ()
	      (slime-mode t)
              (define-key lisp-mode-map (kbd "C-c ;")
	        'slime-insert-balanced-comments)
	      (define-key lisp-mode-map (kbd "C-c M-;")
	        'slime-remove-balanced-comments)
	      (define-key lisp-mode-map (kbd "C-c M-s")
	                  'slime-scratch)
              (rainbow-delimiters-mode 1)
	      (require 'info-look)
	      (info-lookup-add-help
	       :mode 'lisp-mode
	       :regexp "[^][()'\" \t\n]+"
	       :ignore-case t
	       :doc-spec '(("(ansicl)Symbol Index" nil nil nil)))
              (smartparens-mode t)))

(add-hook 'slime-repl-mode-hook
	  #'(lambda ()
	      (paredit-mode 1)
              (rainbow-delimiters-mode 1)
              (define-key lisp-mode-map (kbd "C-c ;")
	        'slime-insert-balanced-comments)
	      (define-key lisp-mode-map (kbd "C-c M-;")
	        'slime-remove-balanced-comments)
	      (define-key lisp-mode-map (kbd "C-c M-s")
	        'slime-scratch)))

(add-hook 'emacs-lisp-mode-hook
	  #'(lambda ()
	      ;;(init-setup-lisp-bindings emacs-lisp-mode-map)
	      (paredit-mode 1)
	      (smartparens-mode t)
              (rainbow-delimiters-mode 1)))

(add-hook 'lisp-interaction-mode-hook
	  #'(lambda ()
	      ;;(init-setup-lisp-bindings lisp-interaction-mode-map)
	      (paredit-mode 1)
              (rainbow-delimiters-mode 1)
	      (smartparens-mode t)))

(add-hook 'paredit-mode-hook
          #'(lambda ()
              (define-key paredit-mode-map (kbd ")")
                (lambda ()
                  (interactive)
                  (when abbrev-mode
                    (expand-abbrev))
                  (paredit-close-round)))))

;; Trigger hooks for lisp-interaction-mode if emacs is started with -[Qq]
(add-hook 'after-init-hook
          (lambda ()
            (save-excursion
              (set-buffer "*scratch*")
              (run-hooks 'lisp-interaction-mode-hook))))

(setq inferior-lisp-mode-hook nil)
(add-hook 'inferior-lisp-mode-hook '(inferior-slime-mode t))
(setq scheme-program-name "guile"
      slime-startup-animation nil
      slime-complete-symbol-function 'slime-simple-complete-symbol
      common-lisp-hyperspec-root (if (file-exists-p "/usr/share/doc/hyperspec")
				     "file:///usr/share/doc/hyperspec/"
				   "http://www.lispworks.com/documentation/HyperSpec/")
      nrepl-sync-request-timeout 60
      stumpwm-shell-program "~/bin/stumpish"
      arc-program-name "bin/arc --no-rl"
      cider-boot-parameters "cider repl -s wait"
      show-paren-context-when-offscreen t)

(defun init-sp-fix-pairs ()
  "Remove silly pairing of quotes and ticks from `sp-pair-list'`"
  (mapc (lambda (x) (setq sp-pair-list (remove x sp-pair-list)))
        '(("'" . "'") ("\"" . "\"") ("`" . "`"))))

(defun init-setup-lisp-bindings (map)
  "Configure various bindings for editing lisp in MAP."
  (define-key map (kbd "C-t") 'transpose-sexps)
  (define-key map (kbd "C-M-t") 'transpose-chars)
  (define-key map (kbd "C-p") 'backward-sexp)
  (define-key map (kbd "C-M-h") 'previous-line)
  (define-key map (kbd "C-n") 'forward-sexp)
  (define-key map (kbd "C-M-n") 'next-line))

(defun init-make-sp-nice ()
  "Fix annoying things with smartparens"
  (init-sp-fix-pairs)
  (sp-use-paredit-bindings)
  (smartparens-strict-mode t))

(require 'clojure-mode)
(require 'clojure-essential-ref)
(require 'nov)
(require 'flycheck-pos-tip)
(require 'flycheck-clj-kondo)
(eval-after-load 'flycheck
  '(setq flycheck-display-errors-function #'flycheck-pos-tip-error-messages))
(add-hook 'clojure-mode-hook (lambda ()
                               (clj-refactor-mode 1)
                               (smartparens-strict-mode 1)
                               (flycheck-mode 1)
                               (cljr-add-keybindings-with-prefix "C-c C-r")
                               (yas-minor-mode 1)))
(add-hook 'clojurescript-mode-hook (lambda ()
                                     (smartparens-strict-mode 1)
                                     (cljr-add-keybindings-with-prefix "C-c C-r")))

(add-hook 'clojure-ts-mode-hook #'cider-mode)
(add-hook 'clojure-ts-mode-hook #'enable-paredit-mode)
(add-hook 'clojure-ts-mode-hook #'rainbow-delimiters-mode)
(add-hook 'clojure-ts-mode-hook #'clj-refactor-mode)
(add-hook 'clojure-ts-mode-hook #'yas-minor-mode-on)
(add-hook 'clojure-ts-mode-hook (lambda () (cljr-add-keybindings-with-prefix "C-c C-r")))
(add-hook 'clojure-ts-mode-hook (lambda () (flycheck-mode 1)))

(require 'smartparens-config)
(add-hook 'cider-mode-hook 'smartparens-mode)
(cider-auto-test-mode)
(add-hook 'smartparens-enabled-hook 'init-make-sp-nice)
(eval-after-load 'flycheck (lambda () (flycheck-pos-tip-mode)))

(setq cider-repl-history-file (expand-file-name "cider-history" user-emacs-directory)
      clojure-essential-ref-nov-epub-path (expand-file-name "media/books/software/Clojure_The_Essential_Reference_v30_MEAP.epub" org-directory)
      clojure-essential-ref-default-browse-fn #'clojure-essential-ref-nov-browse)

(add-to-list 'auto-mode-alist '("\\.lisp\\'" . common-lisp-mode))
;; for clojure-essential-ref
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(add-hook 'cider-mode-hook
          (lambda ()
            (define-key cider-mode-map (kbd "C-c C-d C-d") 'clojure-essential-ref)
            (define-key cider-mode-map (kbd "C-c C-d C-M-d") 'cider-doc)))

(add-hook 'cider-repl-mode-hook
          (lambda ()
            (define-key cider-mode-map (kbd "C-c C-d C-d") 'clojure-essential-ref)
            (define-key cider-mode-map (kbd "C-c C-d C-M-d") 'cider-doc)))

(put-clojure-indent 'facts 'defun)
(put-clojure-indent 'fact 'defun)
(put-clojure-indent 'async 'defun)

;; for sdkman
(require 'exec-path-from-shell)
(when (daemonp)
  (exec-path-from-shell-initialize))

;;; init-lisp.el ends here
