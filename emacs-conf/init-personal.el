;;; init-personal.el --- Wrapper for handling sensitive information.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Where ~/.personal.el is in the form:
;;(defvar personal-mail-password "password"
;;  "Password for mail server.")
;;
;;(setq smtpmail-default-smtp-server "mail.server.com"
;;      user-mail-address "user@mail.server.com"
;;      user-full-name "User Name"
;;      inhibit-startup-echo-area-message "user"
;;      erc-server "irc.server.net"
;;      erc-nick "nick"
;;      erc-user-full-name "full name"
;;      personal-network-hosts '("host1" "host2")
;;      personal-formal-name "formal name"
;;      personal-postal-address '("line 1" "line 2"))
;;      personal-alternate-mail-sources '(("server" "username"
;;                                         "password" port stream)))

;; Utilities for use in .personal.el
(require 'cl)

(defcustom personal-mail-password ""
  "Password for mail server."
  :type 'string
  :group 'init)

(defcustom personal-network-hosts '()
  "The names of the hosts which you have on your network."
  :type 'sexp
  :group 'init)

(defcustom personal-formal-name ""
  "The name by which you are formally known."
  :type 'string
  :group 'init)

(defcustom personal-postal-address '()
  "Your current postal address.
This should be a list of strings, with each line of your address
a separate element."
  :type 'sexp
  :group 'init)

(defcustom personal-muse-blog-publish-path ""
  "Path to publish muse-blog"
  :type 'string
  :group 'init)

(defcustom personal-muse-blog-url ""
  "URL to the muse blog."
  :type 'string
  :group 'init)

(defcustom personal-muse-blog-name ""
  "Name of the muse blog."
  :type 'string
  :group 'init)

(defcustom personal-muse-blog-desc ""
  "Description to the muse blog."
  :type 'string
  :group 'init)

(defcustom personal-muse-blog-rss-entries 20
  "Description to the muse blog."
  :type 'string
  :group 'integer)

(let ((user-personal-conf (expand-file-name ".personal.el" (getenv "HOME"))))
  (unless (and (file-exists-p user-personal-conf)
               (load user-personal-conf t t))
    (message "%s: %s" user-personal-conf "File not found")))

(defun personal-postcode ()
  "Return the postcode from the postal address.
Note that this makes assumptions regarding the format of the
postal address."
  (car (nreverse (split-string (car (cdr personal-postal-address))))))

;;; init-personal.el ends here
