;;; init-notify.el --- Notifications

;; Copyright (C) 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defvar init-notify-allow-notify t
  "Whether to allow notifications.")

(defcustom init-notify-function 'init-notify-file-xmobar
  "Type of notification to use."
  :type 'function
  :group 'init)

(defun init-notify-toggle (&optional allow)
  "Toggle allowance of notifications."
  (interactive "P")
  (setq init-notify-allow-notify
        (if (numberp allow)
            (> allow 0)
          (not init-notify-allow-notify))))


;; summary notification &optional importance &rest args
(defun init-notify (summary body &optional important &rest args)
  "Present notification via `init-notify-type'."
  (funcall init-notify-function summary body important args))

(defun init-notify-stumpwm (summary body &optional important args)
  "Provide notifications to StumpWM via the notifications module."
  (init-stumpwm-run-commands
   (format "notifications-delete-substring %s:"
           summary))
  (when important
    (init-stumpwm-notify (format "%s: %s" (if summary
                                              (concat summary ": ")
                                            "")
                                 body))))

(defun init-notify-file-xmobar (summary body &optional important args)
  "Output text in a format suitable for xmobar."
  (let ((colourise (car args))
        (file (cadr args)))
    (when (file-exists-p file)
      (delete-file file))
    (when important
      (with-temp-file file
        (insert (format "%s%s%s%s"
                        (if (eq colourise 't)
                            "<fc=lightskyblue1>"
                          "")
                        (if summary
                            (format "%s: " summary)
                          "")
                        (cond
                         ((eq colourise 'prop)
                          (init-notify-format-for-xmobar body))
                         (t
                          body))
                        (if (eq colourise 't)
                            "</fc>"
                          "")))))))

(defun init-notify-file-dzen (summary body &optional important args)
  "Output text in a format suitable for dzen2."
  (let ((colourise (car args))
        (file (cadr args)))
    (when (file-exists-p file)
      (delete-file file))
    (when important
      (with-temp-file file
        (insert (format "%s%s%s%s"
                        (if (eq colourise 't)
                            "^fg(lightskyblue1)"
                          "")
                        (if summary
                            (format "%s: " summary)
                          "")
                        (cond
                         ((eq colourise 'prop)
                          (init-notify-format-for-dzen body))
                         (t
                          body))
                        (if (eq colourise 't)
                            "^fg()"
                          "")))))))

(defun init-notify-dbus (summary body &optional important args)
  "Present notifications via dbus.
Use `init-notify-toggle' to specify whether notifications should
be displayed or not."
  (when init-notify-allow-notify
    (require 'dbus)
    (dbus-call-method
     ;; bus and service
     :session "org.freedesktop.Notifications"
     ;; path and interface
     "/org/freedesktop/Notifications" "org.freedesktop.Notifications"
     ;; method and application name
     "Notify" "GNU Emacs"
     ;; do not replace other notifications
     0
     ;; icon
     "/usr/local/share/icons/hicolor/48x48/apps/emacs.png"
     ;; notification summary and body
     (or summary "Unknown") body
     ;; no actions, no hints and default timeout.
     '(:array)
     '(:array :signature "{sv}")
     ':int32 -1)))

(defun init-notify-format-for-xmobar (text &optional colour)
  "Format text in a format suitable for xmobar."
  (let ((res "")
        (inprop nil))
    (dotimes (i (length text))
      (setq res
            (concat
             res
             (cond
              ((and (get-text-property i 'face text)
                    (not inprop))
               (setq inprop t)
               (format "<fc=%s>" (if (stringp colour)
                                     colour
                                   "grey22,lightskyblue1")))
              ((and (not (get-text-property i 'face text))
                    inprop)
               (setq inprop nil)
               "</fc>"))
             (substring-no-properties text i (1+ i)))))
    res))

(defun init-notify-format-for-dzen (text &optional format)
  "Format text in a format suitable for dzen."
  (let ((res "")
        (inprop nil))
    (dotimes (i (length text))
      (setq res
            (concat
             res
             (cond
              ((and (get-text-property i 'face text)
                    (not inprop))
               (setq inprop t)

               (if (stringp format)
                   format
                 "^fg(grey20)^bg(lightskyblue1)"))
              ((and (not (get-text-property i 'face text))
                    inprop)
               (setq inprop nil)
               "^fg()^bg()"))
             (substring-no-properties text i (1+ i)))))
    res))

;;; init-notify.el ends here
