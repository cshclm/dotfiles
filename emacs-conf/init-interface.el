;;; init-interface.el: Face and appearance configuration.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

(setq solarized-distinct-fringe-background nil)
(setq solarized-high-contrast-mode-line nil)
(setq solarized-use-less-bold nil)
(setq solarized-use-more-italic nil)
(setq solarized-emphasize-indicators t)
(setq solarized-scale-org-headlines nil)
(setq solarized-use-variable-pitch nil)

;;(require 'solarized-palettes)
;;(require 'solarized-dark-theme)
;;
;;(load-theme 'solarized-dark)
;;(require 'zenburn-theme)

(add-to-list 'custom-safe-themes "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c")
(require 'catppuccin-theme)
(setq catppuccin-flavor 'macchiato) ;; or 'latte, 'macchiato, or 'mocha
(load-theme 'catppuccin :no-confirm)

(defun init-catppuccin-get-color (color)
  (alist-get color catppuccin-macchiato-colors))

;; For compat reasons?
(defun init-using-font-backend ()
  "Determine whether font-backend support is enabled."
  (catch 'backend-enabled
    (dolist (elt (frame-parameters))
      (when (eq (car elt) 'font-backend)
	(throw 'backend-enabled t)))))

(set-frame-font "Iosevka-16" nil)
(add-to-list 'default-frame-alist '(font . "Iosevka-16"))
;;(init-set-font-backend-font)

(set-face-attribute 'default nil :family "Iosevka")
(set-face-attribute 'variable-pitch nil :family "Iosevka Aile")
(set-face-attribute 'fixed-pitch nil :family "Iosevka")
(setq-default line-spacing 0.1)

;;; init-interface.el ends here

