;;; init-ruby.el --- Ruby Configuration

;; Copyright (C) 2014 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(add-to-list 'load-path (expand-file-name "enhanced-ruby-mode" init-library-path))
(add-to-list 'load-path (expand-file-name "robe" init-library-path))

(autoload 'inf-ruby "inf-ruby" "Run an inferior Ruby process" t)
(autoload 'robe-mode "robe" "Ruby code assistance" t)
(autoload 'robe-ac-setup "robe-ac" "Ruby code assistance - auto complete" t)

(autoload 'enh-ruby-mode "enh-ruby-mode" "Major mode for ruby files" t)
(add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . ruby-mode))

(add-hook 'ruby-mode-hook 'inf-ruby-minor-mode)
(add-hook 'ruby-mode-hook 'robe-mode)
(add-hook 'ruby-mode-hook 'yard-mode)

(add-hook 'ruby-mode-hook (lambda ()
                            (flycheck-mode)))

(setq rbenv-show-active-ruby-in-modeline nil
      inf-ruby-default-implementation "pry")
(require 'rbenv)

(global-rbenv-mode 1)
(require 'rubocop)
(require 'flycheck)

;;; init-anything.el ends here
