;; init-vcs.el -- Various VCS configurations

;; Copyright (C) 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'magit)
(global-set-key (kbd "C-c v") 'magit-status)
(setq magit-last-seen-setup-instructions "1.4.0"
      magit-popup-use-prefix-argument 'popup
      magit-status-sections-hook '(magit-insert-status-headers magit-insert-merge-log magit-insert-rebase-sequence magit-insert-am-sequence magit-insert-sequencer-sequence magit-insert-untracked-files magit-insert-unstaged-changes magit-insert-staged-changes magit-insert-stashes magit-insert-unpushed-to-pushremote magit-insert-unpushed-to-upstream-or-recent magit-insert-unpulled-from-pushremote magit-insert-unpulled-from-upstream)
      ;; --graph is included by default, but is very slow on large repositories
      magit-log-arguments '("-n256" "--decorate"))
(require 'parse-time)
(require 'smerge-mode)

(defadvice magit-commit-amend-assert (around
                                      prevent-magit-commit-amend-because-too-slow
                                      first
                                      (&optional commit)
                                      activate)
  "Don't run `magit-commit-amend-assert' because it takes forever."
  )

;; Temporary workaround to get past breakage in magit-commit-popup (lexical
;; binding issue in package?)
(defun init-magit-commit-popup (&optional args)
  (interactive "P")
  (if args
      (magit-commit-popup args)
    (call-interactively 'magit-commit)))

(add-hook 'magit-mode-hook
          (lambda ()
            (define-key magit-mode-map (kbd "c") 'init-magit-commit-popup)))

(require 'git-link)

(global-set-key (kbd "C-c .") 'git-link)

(defun init-git-link-bitbucket-server (hostname dirname filename _branch commit start end)
  (let* ((alias-matches-p (lambda (elt) (not (string= (car elt) hostname))))
         (matching-aliases (remove-if alias-matches-p init-git-link-bitbucket-server-mirror-aliases))
         (mirror (cdar matching-aliases)))
    (format "https://%s/projects/%s/browse/%s?at=%s#%s"
            (or mirror hostname)
            (if (string-match "^stash/" dirname)
                (replace-regexp-in-string "stash/\\(.*?\\)/\\(.*?\\)" "\\1/repos/\\2" dirname)
              (replace-regexp-in-string "\\(.*?\\)/\\(.*?\\)" "\\1/repos/\\2" dirname))
            filename
            commit
            start)))

;;; init-vcs.el
