;;; init-java.el --- Java configuration

;; Copyright (C) 2015  Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'flymake)
(add-hook 'java-mode-hook 'flymake-mode-on)
(add-hook 'java-mode-hook 'flymake-mode)

(add-hook 'java-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil
                  tab-width 4)
            (define-key java-mode-map (kbd "C-c C-c") 'compile)))

(defun init-lsp-find-definition (&optional arg)
  (interactive "P")
  (if arg
      (funcall-interactively 'lsp-find-references)
    (funcall-interactively 'lsp-find-definition)))

(add-hook 'kotlin-mode-hook
          (lambda ()
            (define-key kotlin-mode-map (kbd "M-.") 'init-lsp-find-definition)))

(setq company-minimum-prefix-length 1
      company-idle-delay 0.0)

(defun init-idea-find-file (file line col)
  (message "%s" (type-of file))
  (message "%s" (type-of line))
  ;; Load the file
  (find-file file)

  ;; Jump to the same point as in IntelliJ
  ;; Unfortunately, IntelliJ doesn't always supply the values
  ;; depending on where the open is invoked from; e.g. keyboard 
  ;; works, tab context doesn't
  (when (numberp line)
    (goto-char (point-min))
    (forward-line (1- line))
    (forward-char (1- col)))

  ;; Raise/focus our window; depends on the windowing system
  (if (string-equal system-type "darwin")
      (ns-do-applescript "tell application \"Emacs\" to activate")
    (raise-frame))

  ;; Automatically pick up changes made in IntelliJ
  (auto-revert-mode t))

(require 'kotlin-mode)
(require 'lsp-java)
(add-hook 'java-mode-hook #'lsp)
(add-hook 'kotlin-mode-hook #'lsp)

(with-eval-after-load 'lsp-mode
  (setq lsp-modeline-diagnostics-scope :workspace
        lsp-kotlin-compiler-jvm-target "11"))

(require 'dap-mode)
(add-hook 'dap-stopped-hook
          (lambda (arg) (call-interactively #'dap-hydra)))

(setq lsp-java-vmargs '("-XX:+UseParallelGC" "-XX:GCTimeRatio=4" "-XX:AdaptiveSizePolicyWeight=90" "-Dsun.zip.disableMemoryMapping=true" "-Xmx6G" "-Xms1G")
      lsp-java-configuration-maven-user-settings (expand-file-name "settings.xml" "~/.m2"))

;;(use-package lsp-mode
;;  :init
;;  (setq lsp-prefer-flymake nil)
;;  :demand t
;;  :after jmi-init-platform-paths)
;;
;;(use-package lsp-ui
;;  :config
;;  (setq lsp-ui-doc-enable nil
;;        lsp-ui-sideline-enable nil
;;        lsp-ui-flycheck-enable t)
;;  :after lsp-mode)
;;
;;(use-package dap-mode
;;  :config
;;  (dap-mode t)
;;  (dap-ui-mode t))
;;
;;(use-package lsp-java
;;  :init
;;  (defun jmi/java-mode-config ()
;;    (setq-local tab-width 4
;;                c-basic-offset 4)
;;    (toggle-truncate-lines 1)
;;    (setq-local tab-width 4)
;;    (setq-local c-basic-offset 4)
;;    (lsp))
;;
;;  :config
;;  ;; Enable dap-java
;;  (require 'dap-java)
;;
;;  ;; Support Lombok in our projects, among other things
;;  (setq lsp-java-vmargs
;;        (list "-noverify"
;;              "-Xmx2G"
;;              "-XX:+UseG1GC"
;;              "-XX:+UseStringDeduplication"
;;              (concat "-javaagent:" jmi/lombok-jar)
;;              (concat "-Xbootclasspath/a:" jmi/lombok-jar))
;;        lsp-file-watch-ignored
;;        '(".idea" ".ensime_cache" ".eunit" "node_modules"
;;          ".git" ".hg" ".fslckout" "_FOSSIL_"
;;          ".bzr" "_darcs" ".tox" ".svn" ".stack-work"
;;          "build")
;;
;;        lsp-java-import-order '["" "java" "javax" "#"]
;;        ;; Don't organize imports on save
;;        lsp-java-save-action-organize-imports nil
;;
;;        ;; Formatter profile
;;        lsp-java-format-settings-url
;;        (concat "file://" jmi/java-format-settings-file))
;;
;;  :hook (java-mode   . jmi/java-mode-config)
;;
;;  :demand t
;;  :after (lsp lsp-mode dap-mode jmi-init-platform-paths))
;;
;;  (define-key lsp-ui-mode-map
;;    [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
;;  (define-key lsp-ui-mode-map
;;    [remap xref-find-references] #'lsp-ui-peek-find-references)

(defun init-open-file-in-idea ()
  (interactive)
  (async-shell-command (concat "idea " (buffer-file-name (current-buffer)))))

(define-key java-mode-map (kbd "C-c j") 'init-open-file-in-idea)

;;(require 'lsp-java-boot)
;;(require 'lsp)
;;(require 'lsp-mode)
;;(require 'dap-java)

;; to enable the lenses
;;(add-hook 'lsp-mode-hook #'lsp-lens-mode)
;;(add-hook 'lsp-mode-hook #'lsp-ui-mode)
;;(add-hook 'java-mode-hook #'lsp-java-boot-lens-mode)

;; groovy mode seems broken
(add-to-list 'auto-mode-alist '("\\.groovy\\'" . java-mode))

(setq eclim-executable "eclim"
      ac-dwim t
      ;;dap-java-test-runner (expand-file-name "junit-platform-console-standalone.jar" "~/.emacs.d/eclipse.jdt.ls/server/test-runner/")
      )

;;(setq lsp-kotlin-language-server-path "kotlin-language-server"
;;      lsp-kotlin-compiler-jvm-target "1.11"
;;      lsp-keymap-prefix "C-c C-s"
;;      lsp-ui-peek-list-width 130
;;      lsp-java-content-provider-preferred "fernflower")

(require 'lsp-java)
(add-hook 'java-mode-hook #'lsp)

(require 'lsp-mode)
;;((lsp-mode . lsp-enable-which-key-integration))
(require 'hydra)
(require 'company)
(require 'lsp-ui)
(require 'which-key)
(which-key-mode)
(require 'lsp-java)
(add-hook 'java-mode-hook 'lsp)
(require 'dap-mode)
         ;:after lsp-mode :config (dap-auto-configure-mode))
(require 'dap-java)
;;(use-package helm-lsp)
;;(use-package helm
;;  :config (helm-mode))
(require 'lsp-treemacs)

;;(require 'lsp-java-boot)

;; to enable the lenses
(add-hook 'lsp-mode-hook #'lsp-lens-mode)
;;(add-hook 'java-mode-hook #'lsp-java-boot-lens-mode)
(setq lsp-ui-sideline-show-code-actions t)

(require 'gradle-mode)
(gradle-mode 1)
(setf gradle-use-gradlew t)
(setf gradle-gradlew-executable "./gradlew")

;;; init-java.el ends here
