;;; init-buffer.el --- Buffer management and navigation configurations.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(autoload 'ibuffer "ibuffer" "List buffers." t)

(temp-buffer-resize-mode t)

(setq compilation-window-height (/ (- (frame-height) 2) 3)
      temp-buffer-max-height (lambda (buffer) (/ (- (frame-height) 2) 3))
      compilation-scroll-output t
      completions-detailed t
      next-error-message-highlight t)

(add-hook 'compilation-finish-functions
          (lambda (buf str)
            (if (string-match (buffer-name) "*Compilation*")
                (goto-char (point-max)))))

(require 'ace-window)

(and (init-depends "keybindings")
     (global-set-key (kbd "C-x C-b") 'ibuffer-list-buffers)
     (global-set-key (kbd "C-x o") 'ace-window)
     (global-set-key (kbd "M-o") 'ace-window)
     (setq aw-keys '(?h ?t ?n ?s ?u ?e ?o ?a ?d ?i ?1 ?2 ?3)))

;; browse-kill-ring
(autoload 'browse-kill-ring "browse-kill-ring" "Display items in the `kill-ring' in another buffer." t)
(setq browse-kill-ring-display-duplicates nil)
(add-hook 'browse-kill-ring-hook
	  (lambda ()
	    (when (= (length kill-ring) 0)
	      (message "Kill ring is empty")
	      (run-at-time 2.0 nil 'browse-kill-ring-quit))))

(add-to-list 'display-buffer-alist '("\\*\\(Warnings\\)\\*"
                                     (display-buffer-reuse-window
                                      display-buffer-reuse-mode-window
                                      display-buffer-in-previous-window
                                      display-buffer-in-side-window)
                                     (window-height . 0.10)
                                     (side . bottom)
                                     (slot . 1)))

(add-to-list 'display-buffer-alist '("\\*\\(Org Agenda(d)\\)\\*"
                                     (display-buffer-reuse-window
                                      display-buffer-reuse-mode-window)
                                     (window-width . 0.5)
                                     (side . right)
                                     (slot . 1)))

(add-to-list 'display-buffer-alist '("\\*\\(Org Agenda(h)\\)\\*"
                                     (display-buffer-reuse-window
                                      display-buffer-reuse-mode-window)
                                     (window-width . 0.5)
                                     (side . left)
                                     (slot . 1)))

;;; init-buffer.el ends here
