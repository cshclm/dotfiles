;;; init-manifest.el --- Configuration manifests to be loaded on initialisation.

;; Copyright (C) 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defvar init-manifest-name nil
  "Name of the manifest to be used.")

(defcustom init-target-manifest nil
  "The default configuration manifest.
Its value should be a list of strings representing the
configuration(s) to load.  If set to nil, the manifest will be
guessed automatically."
  :type 'string
  :group 'init)

(defmacro init-define-manifest (name confs &optional base)
  "Create a new manifest.
NAME is the name of the manifest.  CONFS is a list of
configurations to load.  BASE is the name of an optional existing
manifest upon which the manifest to be created should be based."
  (let ((symname (intern (concat "init-manifest-" (symbol-name name))))
        (symbase (intern (concat "init-manifest-" (symbol-name base)))))
    `(defvar ,symname
       ,(if base
            `(append ,symbase ,confs)
          confs))))

(init-define-manifest minimum '("package" "functions" "interface" "personal" "ivy"
                                "yasnippet" "misc" "keybindings" "buffer" "tramp" "dired"
                                "interface"))

(init-define-manifest standard '("w3m" "lang-shell" "eshell" "lang-lisp" "linguistics" "beancount")
                    minimum)

(init-define-manifest org '("org") minimum)

(init-define-manifest full '("vcs" "lang-c-mode" "lang-python" "lang-ruby"
                           "org" "notmuch" "lang-tex" "lang-perl" "lang-haskell"
                           "lang-sql" "lang-webdev" "lang-java" "quicklinks"
                           "multiple-cursors" "tree-sitter" "smudge")
                    standard)

(init-define-manifest editor '("persist") full)

(init-define-manifest work '("w3m" "org" "webdev" "lang-sql" "vcs"
                             "multi-mode" "notmuch" "lang-perl")
                    minimum)

(defun init-set-manifest (manifest)
  (setq init-manifest-name (symbol-name manifest)
        init-target-manifest
        (symbol-value (intern (concat "init-manifest-" (symbol-name manifest))))))

(defun init-manifest ()
  "Return the manifest to be used.
If `manifest-override' is nil, attempt to determine a suitable
manifest for the current environment."
  (cond
   (init-target-manifest
    (if (listp init-target-manifest)
        init-target-manifest
      (symbol-value init-target-manifest)))
   ((eq system-type 'gnu/linux)
    (init-set-manifest 'full))
   ((eq system-type 'darwin)
    (init-set-manifest 'full))
   (t
    (init-set-manifest 'minimum))))

;;; init-manifest.el ends here
