(require 'yasnippet)
(setq yas-snippet-dirs (list (expand-file-name "snippets" user-emacs-directory)))
(yas-reload-all)

(yas-global-mode 1)
