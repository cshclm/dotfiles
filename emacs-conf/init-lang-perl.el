;;; init-perl.el --- Configuration for perl development.

;; Copyright (C) 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'cl)

;; Remove perl-mode from `interpreter-mode-alist'.
(setq interpreter-mode-alist
      (remove-if (lambda (x) (eq (cdr x) 'perl-mode))
                 interpreter-mode-alist))

(add-to-list 'auto-mode-alist '("\\.pl\\'" . cperl-mode))
(add-to-list 'auto-mode-alist '("\\.pm\\'" . cperl-mode))
(add-to-list 'interpreter-mode-alist '("perl" . cperl-mode))

(add-hook 'cperl-mode-hook
          (lambda ()
            ;; Redefine keys which are shadowed by '<prefix> C-h'.
            (cperl-define-key "\C-c\C-ia" 'cperl-toggle-autohelp)
            (cperl-define-key "\C-c\C-ip" 'cperl-perldoc)
            (cperl-define-key "\C-c\C-iP" 'cperl-perldoc-at-point)

            (cperl-define-key "\C-c\C-iF" 'cperl-info-on-command
                              [(control c) (control h) F])
            (cperl-define-key "\C-c\C-if" 'cperl-info-on-current-command
                              [(control c) (control h) f])
            (cperl-define-key "\C-c\C-iv"
                              ;;(concat (char-to-string help-char) "v") ; does not work
                              'cperl-get-help
                              [(control c) (control h) v])
            (cperl-define-key "\C-c\C-c"
                              (lambda ()
                                (interactive)
                                (init-command-on-buffer
                                 (concat "perl -I" init-project-root " -cw")
                                 "*Perl Check*")))
            (cperl-define-key "\C-c\C-m"
                              (lambda ()
                                (interactive)
                                (init-command-on-project
                                 (concat "make test")
                                 "*Perl Test*")))))

;;; init-perl.el ends here
