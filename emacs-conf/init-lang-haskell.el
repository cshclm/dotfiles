;;; init-haskell.el --- Configuration for editing Haskell code

;; Copyright (C) 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'haskell-mode)

(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

(add-hook 'haskell-mode-hook (lambda () (subword-mode 1)))

(when (init-depends "keybindings")
  (add-hook 'haskell-mode-hook
            (lambda ()
              (define-key haskell-mode-map (kbd "C-c f") 'haskell-hoogle)
              (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
              (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
              (define-key haskell-mode-map (kbd "C-c C-n C-t") 'haskell-process-do-type)
              (define-key haskell-mode-map (kbd "C-c C-n C-i") 'haskell-process-do-info)
              (define-key haskell-mode-map (kbd "C-c C-n C-c") 'haskell-process-cabal-build)
              (define-key haskell-mode-map (kbd "C-c C-n c") 'haskell-process-cabal)
              (define-key haskell-mode-map (kbd "C-c C-o") 'haskell-compile)
              )))


;;; init-haskell.el ends here
