;; -*- mode: emacs-lisp -*-
;; emacs --- Load and manage a modular Emacs configuration.

;; Copyright (C) 2007, 2008, 2009, 2010 Chris Mann

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defgroup init nil "Custom emacs initialisation."
  :group nil
  :prefix "init-")

(defcustom init-configuration-path "~/conf/emacs-conf"
  "The path in which emacs and library configurations are stored."
  :type 'directory
  :group 'init)

(defcustom init-library-path "~/.emacs.d/lib"
  "The path in which elisp libraries are stored."
  :type 'directory
  :group 'init)

(defcustom init-config-prefix "init-"
  "Prefix used in configuration filenames."
  :type 'string
  :group 'init)

(defvar init-loaded-list '()
  "List of configurations which have been successfully loaded.")

(defvar init-error-list '()
  "List of configuration names which loading has resulted in an error.")

(add-to-list 'load-path init-library-path)
(add-to-list 'load-path (expand-file-name "cshclm-emacs" init-library-path))
(load (expand-file-name "manifest.el" init-configuration-path))

(defun init-config-loaded-p (config)
  "Determine whether CONFIG is loaded. 
Attempt to load CONFIG if it is not already loaded.  If CONFIG
has been loaded successfully, return non-nil.  Otherwise, return
nil.

CONFIG must be a part of the manifest to be loaded.  See `init-manifest'."
  (or (member config init-loaded-list)
      (and (member config (init-manifest)) (init-load config))))

(defun init-depends (config &optional required)
  "Indicate whether the depended file, CONFIG, has been loaded.
Return non-nil if CONFIG has been successfully loaded, nil
otherwise.  If the optional argument REQUIRED is non-nil and
CONFIG could not be loaded an error is raised."
  (or (init-config-loaded-p config)
      (and required (error "%s could not be loaded" config))))

(defun init-error (err-file err)
  "Display an error message and the configuration in which it occured.
Error should be in the format (config-name . error-message)."
  (message "Error loading %s%s: %s" init-config-prefix err-file
           (error-message-string err)))

(defun init-load (configuration)
  "Load the specified configurations from `config-configuration-path'.
CONFIGURATION is a string whose value should match the filename
to be loaded, with or without the '.elc or .el' suffix."
  (interactive
   (list
    ;; Completion for available configurations.
    (completing-read
     "Configuration: "
     (mapcar (lambda (file) (substring file 5 -3))
	     (directory-files init-configuration-path nil
                              (concat init-config-prefix ".+\\.el$")))
     nil nil)))
  (condition-case err
      (let ((absolute-path (expand-file-name
                            (concat init-config-prefix configuration ".el")
                            init-configuration-path)))
	(and (file-exists-p absolute-path)
	     (load absolute-path)
	     (add-to-list 'init-loaded-list configuration)))
    ;; Handle initialisation errors by storing information in a list to be
    ;; processed later.
    (error (if (interactive-p)
	       (init-error
		(expand-file-name configuration
				  init-configuration-path)
		(error-message-string err))
	     (add-to-list 'init-error-list (cons configuration err) t)
	     nil))))

(defun init-initialise ()
  "Load initialisation files."
  (setq init-loaded-list nil)
  (setq init-error-list nil)
  (mapc 'init-load (init-manifest))
  (when init-error-list
    (setq inhibit-startup-echo-area-message user-login-name)
    (add-hook 'after-init-hook
	      #'(lambda ()
		  (dolist (cur-error init-error-list)
		    (init-error (car cur-error)
				(cdr cur-error)))) t)))

(init-depends "personal")

(init-initialise)

;;; emacs ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("51ec7bfa54adf5fff5d466248ea6431097f5a18224788d0bd7eb1257a4f7b773" "f366d4bc6d14dcac2963d45df51956b2409a15b770ec2f6d730e73ce0ca5c8a7" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" default)))
