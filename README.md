My dotfiles.

Notable items:

 - Very extensive Emacs configuration with modular loading system
 - XMonad configuration with configuration for custom extension for quickly accessing URLs
 - zsh configuration
