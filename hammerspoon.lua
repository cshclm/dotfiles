-- Convert a lua table into a lua syntactically correct string
function table_to_string(val, name, skipnewlines, depth)
    skipnewlines = skipnewlines or false
    depth = depth or 0

    local tmp = string.rep(" ", depth)

    if name then tmp = tmp .. name .. " = " end

    if type(val) == "table" then
        tmp = tmp .. "{" .. (not skipnewlines and "\n" or "")

        for k, v in pairs(val) do
            tmp =  tmp .. table_to_string(v, k, skipnewlines, depth + 1) .. "," .. (not skipnewlines and "\n" or "")
        end

        tmp = tmp .. string.rep(" ", depth) .. "}"
    elseif type(val) == "number" then
        tmp = tmp .. tostring(val)
    elseif type(val) == "string" then
        tmp = tmp .. string.format("%q", val)
    elseif type(val) == "boolean" then
        tmp = tmp .. (val and "true" or "false")
    else
        tmp = tmp .. "\"[inserializeable datatype:" .. type(val) .. "]\""
    end

    return tmp
end

hs.hotkey.bind({"ctrl", "ralt"}, "C", function()
  hs.reload()
end)

function showWindow(app)
  hs.application.launchOrFocus(app)

  screen = hs.screen.mainScreen()
  layout = {
     {app, nil, screen, hs.layout.maximized, nil, nil}
  }
  hs.layout.apply(layout)
end

hs.application.enableSpotlightForNameSearches(true)

hs.hotkey.bind({"ralt","shift"}, "h", function()
  thisScreen = hs.screen.mainScreen()
  for k,v in pairs(hs.screen.allScreens()) do
     if v ~= thisScreen then
        otherScreen = v
     end
  end
  
  window = hs.window.focusedWindow()
  app = window:application()

  layout = {
     {app:name(), nil, otherScreen, hs.layout.maximized, nil, nil}
  }
  hs.layout.apply(layout)
end)

hs.hotkey.bind({"ralt","shift"}, "n", function()
  thisScreen = hs.screen.mainScreen()
  for k,v in pairs(hs.screen.allScreens()) do
     if v ~= thisScreen then
        otherScreen = v
     end
  end
  
  window = hs.window.focusedWindow()
  app = window:application()

  layout = {
     {app:name(), nil, otherScreen, hs.layout.maximized, nil, nil}
  }
  hs.layout.apply(layout)
end)

hs.hotkey.bind({"ralt"}, "1", function()
      showWindow("Emacs")
end)

hs.hotkey.bind({"ralt"}, "2", function()
      showWindow("kitty")
end)

hs.hotkey.bind({"ralt"}, "3", function()
      for k,v in pairs(hs.window.allWindows()) do
        hs.logger.new('myconfig','info').i(v:application():name())
      end
      showWindow("Google Chrome")
end)

hs.hotkey.bind({"ralt"}, "4", function()
      showWindow("IntelliJ IDEA")
end)

hs.hotkey.bind({"ralt"}, "7", function()
      showWindow("Slack")
end)

hs.hotkey.bind({"ralt"}, "8", function()
      showWindow("zoom.us")
end)

hs.hotkey.bind({"ralt"}, "9", function()
      showWindow("Spotify")
end)

hs.alert.show("Config loaded")


hs.hotkey.bind({"ralt"}, "a", function()
      hs.spotify.playpause()
end)

hs.hotkey.bind({"ralt"}, "q", function()
      hs.spotify.next()
end)

hs.hotkey.bind({"ralt"}, ";", function()
      hs.spotify.previous()
end)

hs.hotkey.bind({"ralt"}, "0", function()
  screen = hs.screen.mainScreen()
  layout = {
     {"zoom.us", "Zoom Meeting", screen, hs.layout.maximized, nil, nil}
  }
  hs.layout.apply(layout)
end)

function changeVolume(diff)
  return function()
    local current = hs.audiodevice.defaultOutputDevice():volume()
    local new = math.min(100, math.max(0, math.floor(current + diff)))
    if new > 0 then
      hs.audiodevice.defaultOutputDevice():setMuted(false)
    end
    hs.alert.closeAll(0.0)
    hs.alert.show("Volume " .. new .. "%", {}, 0.5)
    hs.audiodevice.defaultOutputDevice():setVolume(new)
  end
end

function toggleMute(diff)
  return function()
    local isMuted = hs.audiodevice.defaultOutputDevice():muted()
    if isMuted then
      hs.audiodevice.defaultOutputDevice():setMuted(false)
    else 
      hs.audiodevice.defaultOutputDevice():setMuted(true)
    end
  end
end

hs.hotkey.bind({'ralt'}, '.', changeVolume(3))
hs.hotkey.bind({'ralt'}, ',', changeVolume(-3))
hs.hotkey.bind({'ralt'}, '\'', toggleMute())

hs.execute("yabai --start-service", true)
